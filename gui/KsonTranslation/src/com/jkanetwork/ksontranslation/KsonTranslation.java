package com.jkanetwork.ksontranslation;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.io.*;
import com.jkanetwork.kson.*;

public class KsonTranslation {
	
	public static final String SOURCE = "../ThiefBusters/app/src/main/assets/data/locales/";
	
	public static void main(String[] args) throws IOException,KsonException {
		Scanner scan = new Scanner(System.in);
		System.out.print("Language to translate (directory): ");
		String language = scan.nextLine();
		System.out.print("File to translate (name): ");
		String file = scan.nextLine();
		File originFile = new File(SOURCE+language+"/"+file);
		if(!originFile.exists() || originFile.isDirectory()){
			scan.close();
			throw new RuntimeException("The file "+originFile.getAbsolutePath()+" not exists or is a directory");
		}
		System.out.print("Translated language (directory): ");
		String destination = scan.nextLine();
		try {
			makeTranslation(originFile,SOURCE+destination+"/"+file,scan);
		} catch (Exception e) {
			scan.close();
			throw e;
		}
		
	}
	
	private static void makeTranslation(File in, String out,Scanner scan) throws IOException, KsonException{
		FileReader fr = new FileReader(in);
		BufferedReader br = new BufferedReader(fr);
		Map<String,String> map = Kson.parse(br);
		Map<String,String> end = new HashMap<>();
		Iterator<String> it = map.keySet().iterator();
		while(it.hasNext()){
			String key = it.next();
			System.out.print("Entry the translation of " + map.get(key) + " (key="+key+"): ");
			String translation = scan.nextLine();
			end.put(key,translation);
		}
		Kson.save(end, out);
	}
}
