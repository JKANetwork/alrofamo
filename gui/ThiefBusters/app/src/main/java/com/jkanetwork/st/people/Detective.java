package com.jkanetwork.st.people;

import com.jkanetwork.st.people.utils.Rank;

/**
 * Player data in game.
 *
 * @author JKA Network
 */
public class Detective extends Person {
	// Attributes
	/**
	 * Player's rank.
	 */
	private Rank rank;
	/**
	 * Mission completed.
	 */
	private int completed;

	// Builder
	/**
	 * Build the Detective.
	 * 
	 * @param name
	 *            Player name
	 * @param sex
	 *            Player sex
	 * @param rank
	 *            Player rank
	 * @param completed
	 *            Mission completed with a victory
	 */
	public Detective(String name, boolean sex, Rank rank, int completed) {
		super(name, sex);
		this.rank = rank;
		this.completed = completed;
	}

	// Getters
	/**
	 * Get the rank.
	 * 
	 * @return rank
	 */
	public Rank getRank() {
		return this.rank;
	}

	/**
	 * Get the missions completed.
	 * 
	 * @return completed
	 */
	public int getCompleted() {
		return this.completed;
	}

	/**
	 * Increase the completed cases.
	 */
	public void increaseCompleted(){
		this.completed++;
	}
	
	/**
	 * Set a new rank
	 * 
	 * @param rank new rank
	 */
	public void setRank(Rank rank){
		this.rank = rank;
	}
	
	/**
	 * {@inheritDoc}.
	 * 
	 * @return {@inheritDoc}
	 */
	@Override
	public String toString(){
		return super.toString()+". Rank: "+rank+", completed misions: "+completed;
	}
}
