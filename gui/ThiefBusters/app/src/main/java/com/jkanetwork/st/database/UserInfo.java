package com.jkanetwork.st.database;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import com.jkanetwork.kson.*;
import com.jkanetwork.st.people.Detective;
import com.jkanetwork.st.people.Thief;
import com.jkanetwork.st.people.utils.Car;
import com.jkanetwork.st.people.utils.Feature;
import com.jkanetwork.st.people.utils.Hair;
import com.jkanetwork.st.people.utils.Hobby;
import com.jkanetwork.st.people.utils.Rank;
import com.jkanetwork.st.places.City;
import com.jkanetwork.st.play.Case;
import com.jkanetwork.st.play.PC;
import com.jkanetwork.st.utils.Utils;

/**
 * Connection and process no relational database.
 * 
 * @author JKA Network
 * @since 1.0
 * @version 1.5
 */
public class UserInfo implements Serializable{

	private final int NUMOFPARAMS = 13; // TODO Add CLOCK
	private final String[] KEYS = { "NAME", "SEX", "RANK", "SOLVED", "CITY", "CITIES", "THIEF", "ORDER", "PCSEX",
			"PCHAIR", "PCFEATURE", "PCCAR", "PCHOBBY" }; // TODO Add CLOCK

	private Map<String, String> userInfo;
	private String route;

	/**
	 * Create UserInfo.
	 * 
	 * @param route
	 *            route where the file is (KSON format).
	 * @throws BadUserInfoFileException
	 *             When the file not exists or not respect the KSON format
	 */
	public UserInfo(String route) throws BadUserInfoFileException {
		this.route = "/data/data/"+route;
		try {
            File file = new File(this.route);
            if(!file.exists())
                file.createNewFile();
			userInfo = Kson.parse(new BufferedReader(new FileReader(file)));
		} catch (KsonException | IOException e) {
            userInfo=new HashMap<>();
			//throw new BadUserInfoFileException(e);
		}
	}

	/**
	 * Get the current city ID.
	 *
	 * @return current city
	 * @return current city
	 */
	public int getCity(){
		return Integer.parseInt(userInfo.get("CITY"));
	}

	/**
	 * Check that the file has a correct format. TODO Add time
	 * 
	 * @throws BadUserInfoFileException
	 *             when the file is incorrect
	 */
	public void checkFile() throws BadUserInfoFileException {
		boolean in = true;
		boolean format = true;
		if (userInfo.size() == NUMOFPARAMS) {
			for (String s : KEYS) {
				if (!userInfo.containsKey(s)) {
					in = false;
					break;
				}
			}

			Iterator<String> it = userInfo.keySet().iterator();
			if (in) {
				try {
					while (it.hasNext() && format) {
						String temp = it.next();
						switch (temp) {
						case "NAME":
							if (userInfo.get(temp).length() == 0)
								format = false;
							break;
						case "SEX":
						case "PCSEX":
							if (!userInfo.get(temp).equals("M") && !userInfo.get(temp).equals("F")
									&& !userInfo.get(temp).equals("null"))
								format = false;
							break;
						case "RANK":
							Rank.valueOf(userInfo.get(temp));
							break;
						case "SOLVED":
							if (Rank.valueOf(userInfo.get("RANK")).getNeeded() > Integer.parseInt(userInfo.get(temp)))
								format = false;
							else if (Rank.valueOf(userInfo.get("RANK")) != Rank.INTERPOL_BOSS
									&& Rank.valueOf(userInfo.get("RANK")).getTotalCases() < Integer
											.parseInt(userInfo.get(temp)))
								format = false;
							break;
						case "CITY":
						case "THIEF":
						case "ORDER":
							Integer.parseInt(userInfo.get(temp));
							break;
						case "CITIES":
							if (Integer.parseInt(userInfo.get("CITY")) != -1) {
								String[] cities = userInfo.get(temp).split(",");
								int[] citiesInt = new int[cities.length];
								if (cities.length < Case.MIN || cities.length > Case.MAX)
									format = false;
								for (int i = 0; i < cities.length && format; ++i) {
									int tempInt = Integer.parseInt(cities[i]);
									if (Utils.NotInIntArray(tempInt, citiesInt) && tempInt > 0)
										citiesInt[i] = tempInt;
									else
										format = false;
								}
							}
							break;
						case "PCHAIR":
							if (!userInfo.get(temp).equals("null"))
								Hair.valueOf(userInfo.get(temp));
							break;
						case "PCCAR":
							if (!userInfo.get(temp).equals("null"))
								Car.valueOf(userInfo.get(temp));
							break;
						case "PCFEATURE":
							if (!userInfo.get(temp).equals("null"))
								Feature.valueOf(userInfo.get(temp));
							break;
						case "PCHOBBY":
							if (!userInfo.get(temp).equals("null"))
								Hobby.valueOf(userInfo.get(temp));
							break;
						}
					}
				} catch (RuntimeException e) {
					format = false;
				}
			}
		} else {
			in = false;
		}
		if (!in)
			throw new BadUserInfoFileException("The file is incorrect or not exists");
		else if (!format)
			throw new BadUserInfoFileException("The file has a ilegal modification");
	}

	/**
	 * Load a game.
	 * 
	 * @param gd
	 *            GameDatabase where is the information of thieves
	 * @param pc
	 *            computer of the game
	 * @param cities
	 *            path
	 * @return city id where the user is, if it is -1 nothing game is saved
	 *         //MODIFICAR
	 */
	public int loadGame(PC pc, int[] cities) {
		this.loadPC(pc);
        String[] string = userInfo.get("CITIES").split(",");
		for (int i = 0; i < cities.length; ++i)
			cities[i] = Integer.parseInt(string[i]);
		return Integer.parseInt(this.userInfo.get("CITY"));
	}

	public boolean isSaved(){
        if(Integer.parseInt(userInfo.get("CITY"))==-1)
            return false;
        else
            return true;
    }

    public int numOfCities(){
        String[] string = userInfo.get("CITIES").split(",");
        return string.length;
    }

	/**
	 * Load the detective information.
	 * 
	 * @return the detective loaded
	 */
	public Detective loadDetective() {
		boolean sex = false;
		if (userInfo.get("SEX").equals("F"))
			sex = true;
		return new Detective(userInfo.get("NAME"), sex, Rank.valueOf(userInfo.get("RANK")),
				Integer.parseInt(userInfo.get("SOLVED")));
	}

	/**
	 * Load the real thief who stole the monument.
	 * 
	 * @param gd
	 *            gameDatabase where the info is
	 * @return thief loaded
	 */
	public Thief loadThief(GameDatabase gd) {
		if (Integer.parseInt(userInfo.get("THIEF")) != -1)
			return gd.selectThief(Integer.parseInt(userInfo.get("THIEF")));
		return null;
	}

	/**
	 * Load the thief who the detective believes it's the criminal.
	 * 
	 * @param gd
	 *            gameDatabase where the info is
	 * @return thief loaded
	 */
	public Thief loadThiefPC(GameDatabase gd) {
		if (Integer.parseInt(userInfo.get("ORDER")) != -1)
			return gd.selectThief(Integer.parseInt(userInfo.get("ORDER")));
		return null;
	}

	/**
	 * Load a saved PC.
	 * 
	 * @param pc
	 *            where all be saved
	 */
	private void loadPC(PC pc) {
		String sex = this.userInfo.get("PCSEX");
		if (sex.equals("null"))
			sex = null;
		pc.sex = sex;

		String hair = this.userInfo.get("PCHAIR");
		if (hair.equals("null"))
			hair = null;
		pc.hair = hair;

		String car = this.userInfo.get("PCCAR");
		if (car.equals("null"))
			car = null;
		pc.car = car;

		String feature = this.userInfo.get("PCFEATURE");
		if (feature.equals("null"))
			feature = null;
		pc.feature = feature;

		String hobby = this.userInfo.get("PCHOBBY");
		if (hobby.equals("null"))
			hobby = null;
		pc.hobby = null;
	}

	/**
	 * Save the game during it self.
	 * 
	 * @param pc
	 *            PC where the Detective put this clues
	 * @param city
	 *            City where the Detective is
	 * @param thief
	 *            Thief who the detective need to catch
	 * @param order
	 *            Thief who the detective believe need to catch
	 */
	public void save(PC pc, int city, int thief, int order,int[] cities) {
		String sex = pc.sex, car = pc.car, hair = pc.hair, feature = pc.feature, hobby = pc.hobby;
		if (pc.sex == null)
			sex = "null";
		if (pc.car == null)
			car = "null";
		if (pc.feature == null)
			feature = "null";
		if (pc.hair == null)
			hair = "null";
		if (pc.hobby == null)
			hobby = null;
		String ciudades=""+cities[0];
        for(int i=1;i<cities.length;i++){
            ciudades+=","+cities[i];
        }

		userInfo.put("PCSEX", sex);
		userInfo.put("PCHAIR", hair);
		userInfo.put("PCHOBBY", hobby);
		userInfo.put("PCFEATURE", feature);
		userInfo.put("PCCAR", car);
        userInfo.put("CITIES",ciudades);
		userInfo.put("CITY", String.valueOf(city));
		userInfo.put("THIEF", String.valueOf(thief));
		userInfo.put("ORDER", String.valueOf(order));

		try {
			Kson.save(userInfo, this.route);
		} catch (IOException e) {
			throw new RuntimeException("Fatal error", e);
		}
	}

	/**
	 * Save the detective data when the game is finished.
	 * 
	 * @param d
	 *            Detective who is playing
	 */
	public void saveFinished(Detective d) {
		userInfo.put("PCSEX", "null");
		userInfo.put("PCHAIR", "null");
		userInfo.put("PCHOBBY", "null");
		userInfo.put("PCFEATURE", "null");
		userInfo.put("PCCAR", "null");
		userInfo.put("CITY", String.valueOf(-1));
        userInfo.put("CITIES",String.valueOf(-1));
		userInfo.put("THIEF", String.valueOf(-1));
		userInfo.put("ORDER", String.valueOf(-1));
		userInfo.put("RANK", d.getRank().name());
		userInfo.put("SOLVED", String.valueOf(d.getCompleted()));

		try {
			Kson.save(userInfo, this.route);
		} catch (IOException e) {
			throw new RuntimeException("Fatal error", e);
		}
	}

	public void create(Detective d){
        userInfo = new HashMap<>();
		userInfo.put("NAME", d.getName());
        String sex = "M";
        if(d.getSex())
            sex="F";
        userInfo.put("SEX",sex);
        saveFinished(d);
	}
}