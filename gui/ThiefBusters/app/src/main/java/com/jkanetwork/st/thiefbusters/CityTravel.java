package com.jkanetwork.st.thiefbusters;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.jkanetwork.st.frontend.Variables;
import com.jkanetwork.st.people.utils.Level;
import com.jkanetwork.st.places.City;
import com.jkanetwork.st.thiefbusters.methods.Methods;

/**
 * Created by joselucross on 22/03/17.
 */

public class CityTravel extends AppCompatActivity {

    /* El menu */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu); /* El menu está en res/menu/main.xml */
        Methods.menuStart(this,menu);
        return true;
    }
    /* Aquí lo que hacer al dar a cada botón del menu */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.options:
            case R.id.about:
            case R.id.exit:
                return Methods.optionsMenu(item,this);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreate(Bundle savedInstace){
        try {
            super.onCreate(savedInstace);
            switch (Variables.caso.getPlayer().getRank().getLevel()) {
                case EASY:
                    setContentView(R.layout.city_travel_easy);
                    if (Variables.currentCity.getDestination(1) == -1)
                        easyError();
                    else
                        easy();
                    break;
                case MEDIUM:
                    setContentView(R.layout.city_travel_medium);
                    medium();
                    break;
                case HARD:
                    setContentView(R.layout.city_travel_hard);
                    hard();
                    break;
            }
            ((Button) findViewById(R.id.back)).setText(Variables.main.getSentence("BACK"));
        }catch (NullPointerException ex){
            finish();
        }
    }

    private void hard(){
        ((Button)findViewById(R.id.dest5)).setText(Variables.data.getCityName(Variables.currentCity.getDestination(4)));
        medium();
    }

    private void medium(){
        ((Button)findViewById(R.id.dest4)).setText(Variables.data.getCityName(Variables.currentCity.getDestination(3)));
        easy();
    }

    private void easy(){
        ((Button)findViewById(R.id.dest3)).setText(Variables.data.getCityName(Variables.currentCity.getDestination(2)));
        ((Button)findViewById(R.id.dest2)).setText(Variables.data.getCityName(Variables.currentCity.getDestination(1)));
        ((Button)findViewById(R.id.dest1)).setText(Variables.data.getCityName(Variables.currentCity.getDestination(0)));
    }

    private void easyError(){
        ((Button)findViewById(R.id.dest1)).setText(Variables.data.getCityName(Variables.currentCity.getDestination(0)));
        findViewById(R.id.dest2).setVisibility(View.INVISIBLE);
        findViewById(R.id.dest3).setVisibility(View.INVISIBLE);
    }

    private void travel(View view,int id){
        int idcity = Variables.currentCity.getDestination(id);
        Variables.currentCity = Variables.caso.loadCity(idcity,Variables.player,Variables.data,Variables.VARIANTS);

        int warrant;
        if(Variables.caso.getWarrant()==null)
            warrant=-1;
        else
            warrant=Variables.caso.getWarrant().getId();
        Variables.user.save(Variables.caso.getPC(),idcity,Variables.caso.getThief().getId(),warrant,Variables.caso.getCities());

        Intent intent = new Intent(this,CityStart.class);
        setResult(1);
        finish();
    }

    public void dest1(View view) { travel(view,0); }
    public void dest2(View view){
        travel(view,1);
    }
    public void dest3(View view){
        travel(view,2);
    }
    public void dest4(View view){
        travel(view,3);
    }
    public void dest5(View view){
        travel(view,4);
    }

    public void back(View view){
        setResult(0);
        finish();
    }

}
