package com.jkanetwork.st.thiefbusters;

import android.content.Intent;
import android.opengl.Visibility;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jkanetwork.st.frontend.Setup;
import com.jkanetwork.st.frontend.Variables;
import com.jkanetwork.st.places.City;
import com.jkanetwork.st.play.Case;
import com.jkanetwork.st.thiefbusters.methods.Methods;

import java.io.IOException;

public class CityStart extends AppCompatActivity {



    /* El menu */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu); /* El menu está en res/menu/main.xml */
        Methods.menuStart(this,menu);
        return true;
    }
    /* Aquí lo que hacer al dar a cada botón del menu */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.options:
            case R.id.about:
            case R.id.exit:
                return Methods.optionsMenu(item,this);
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onCreate(Bundle bundle){
        if(!Variables.exist) {
            try {
                Setup.reloadUserInfo(this);
                Setup.startVariables(this);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
            Variables.exist=true;
            Variables.caso = Setup.reloadCase();
            Variables.currentCity = Variables.caso.loadCity(Variables.user.getCity(),Variables.player,Variables.data,Variables.VARIANTS);
            Log.v("Not exist","La variable no existe");
        }
        super.onCreate(bundle);
        Log.v("OnCreate","OnCreate is called");
        setContentView(R.layout.city_start);
        ((Button)findViewById(R.id.visit)).setText(Variables.main.getSentence("CITY"));
        ((Button)findViewById(R.id.travel)).setText(Variables.main.getSentence("TRAVEL"));
        ((Button)findViewById(R.id.pc)).setText("PC");
        //TODO Set imagen
        ((TextView)findViewById(R.id.cityDescription)).setText(Variables.currentCity.getDescription());
    }

    public void pcClick(View v){
        Methods.pcClick(v,this);
    }

    public void travelClick(View v){
        Methods.travelClick(v,this);
    }

    public void visitClick(View v){
        Methods.visitClick(v,this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(Variables.captured && requestCode==0){
            try {
                Setup.playerDB();
            }catch (IOException ex){
                Log.e("Exception","Saltada excepción borrando la base de datos",ex);
            }
            Variables.user.saveFinished(Variables.caso.getPlayer());
            this.startActivityForResult(new Intent(this,Main.class),1);
            finish();
        }
        else if(requestCode==2){
            if(resultCode==1){
                finish();
                startActivity(getIntent());
            }
        }
    }

    @Override
    public void onBackPressed() { moveTaskToBack(true);}

//    @Override
//    public void onResume(){
//        super.onResume();
//        Log.v("OnResume","OnResume is called");
//
//    }
}
