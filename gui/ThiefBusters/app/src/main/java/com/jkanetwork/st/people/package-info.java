/**
 * Package with people and their characteristics.
 * 
 * @author JKA Network
 */
package com.jkanetwork.st.people;