package com.jkanetwork.st.people;

import com.jkanetwork.st.people.utils.Job;
import com.jkanetwork.st.places.Place;
/**
 * Person who give the player a testimony (or not) about the Thief
 * 
 * @version 1.0
 * @since 1.0
 * @author JKA Network
 */
public class Witness extends Person{
	//Attributes
	/**
	 * Witness's job (for draw).
	 */
	private Job job;
	/**
	 * Witness's testimony.
	 */
	private String testimony;
	/**
	 * Witness's place, each place is linked with only one witness and vice versa.
	 */
	private Place place;
	
	
	//Builders
	/**
	 * Create a witness.
	 * 
	 * @param name
	 * @param sex
	 * @param job
	 * @param testimony
	 */
	public Witness(String name, boolean sex, Job job, String testimony){
		super(name,sex);
		this.job = job;
		this.testimony = testimony;
	}
	
	//Getters
	/**
	 * Get the job.
	 * 
	 * @return job
	 */
	public Job getJob(){
		return this.job;
	}
	/**
	 * Get the testimony.
	 * 
	 * @return testimony
	 */
	public String getTestimony(){
		return this.testimony;
	}
	/**
	 * Get the place.
	 * 
	 * @return place
	 */
	public Place getPlace(){
		return this.place;
	}
	
	//Setters
	public void setPlace(Place place){
		this.place = place;
	}
	/**
	 * {@inheritDoc}.
	 * 
	 * @return {@inheritDoc} 
	 */
	@Override
	public String toString(){
		return super.toString() + ". Job: " + job + ", testimony: " + testimony;
	}
}
