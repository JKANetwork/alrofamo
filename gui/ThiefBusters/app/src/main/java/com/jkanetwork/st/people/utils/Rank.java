package com.jkanetwork.st.people.utils;

/**
 * The player's rank and its level.
 * 
 * @author JKA Network
 */
public enum Rank {
	INTERPOL_BOSS(Level.HARD, 0, null,70), MARSHAL(Level.HARD, 70, Rank.INTERPOL_BOSS,44), CAPTAIN(Level.MEDIUM, 44,
			Rank.MARSHAL,24), INSPECTOR(Level.MEDIUM, 24, Rank.CAPTAIN,14), SERGEANT(Level.MEDIUM, 14,
					Rank.INSPECTOR,6), DETECTIVE(Level.EASY, 6, Rank.SERGEANT,2), RECRUIT(Level.EASY, 2, Rank.DETECTIVE,0);

	/**
	 * Level associated to the rank.
	 */
	private Level level;
	private int totalCases;
	private Rank next;
	private int needed;

	// Builder
	/**
	 * Provide the rank level.
	 * 
	 * @param level
	 *            Level associated to the rank
	 * @param totalCases
	 *            cases need to solve to upgrade
	 */
	private Rank(Level level, int totalCases, Rank next,int needed) {
		this.level = level;
		this.totalCases = totalCases;
		this.next = next;
		this.needed = needed;
	}
	
	/**
	 * Get the number of cases needed to resolve to upgrade.
	 * 
	 * @return integer with the needed
	 */
	public int getNeeded(){
		return this.needed;
	}

	/**
	 * Get a level associated to the rank.
	 * 
	 * @return this level
	 */
	public Level getLevel() {
		return this.level;
	}

	/**
	 * Get the total cases which user need to ascend to this rank.
	 * 
	 * @return totalCases
	 */
	public int getTotalCases() {
		return this.totalCases;
	}
	
	/**
	 * Get the next rank.
	 * 
	 * @return next rank associated
	 */
	public Rank getNext(){
		return this.next;
	}
}
