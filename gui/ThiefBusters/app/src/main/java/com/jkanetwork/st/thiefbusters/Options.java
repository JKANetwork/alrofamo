package com.jkanetwork.st.thiefbusters;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.jkanetwork.st.frontend.Variables;
import com.jkanetwork.st.thiefbusters.dialogs.NewUserDialog;

/**
 * Created by joselucross on 12/04/17.
 */

public class Options extends AppCompatActivity {

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.options);
        try {
            ((Button) findViewById(R.id.updateInfo)).setText(Variables.main.getSentence("CHANGE"));
        }catch (NullPointerException ex){
            finish();
        }
    }

    public void onUpdatePressed(View view){
        DialogFragment d = new NewUserDialog();
        d.setCancelable(true);
        d.show(getFragmentManager(), "UPDATEUSER");
    }
}
