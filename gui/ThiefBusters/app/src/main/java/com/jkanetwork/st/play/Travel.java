package com.jkanetwork.st.play;

import com.jkanetwork.st.places.City;
import com.jkanetwork.st.places.PartialCity;
import com.jkanetwork.st.places.utils.Continent;
/**
 * Calculates the travel time.
 * 
 * @author JKA Network
 */
public class Travel {
	
	/**
	 * Calculate the time to travel to other city.
	 * 
	 * @param currentCity city of origin
	 * @param nextCity destination city
	 * @return
	 */
	public static int travelTime(City currentCity, PartialCity nextCity){
		Continent currentContinent = currentCity.getContinent();
		Continent nextContinent = nextCity.getContinent();
		int hours;
		
		if(currentCity.getCountry().equals(nextCity.getCountry()))
			hours = 1; //Share country
		else if(currentContinent == nextContinent)
			hours = 2; //Share continent
		else if(currentContinent.getSuperContinent() == nextContinent.getSuperContinent())
			hours = 4; //Share super continent
		else
			hours = 8; //Share the planer :D
		
		return hours;
	}
}
