package com.jkanetwork.st.frontend;

import android.support.v7.app.AppCompatActivity;

import java.io.IOException;
import java.util.Map;
import com.jkanetwork.kson.Kson;
import com.jkanetwork.kson.KsonException;

/**
 * The translated class with the translated texts
 * 
 * @author JKA Network
 */
public class InterfaceText implements Translatable {

	/**
	 * Language of the text.
	 */
	private static String lang = Translatable.LANG; //By default the language is English, all InterfaceText have the same language
													//If the lang change, is necesary restart
	/**
	 * HashMap where the text will be.
	 */
	private Map<String,String> map;
	/**
	 * The name of the kson file where the translation is.
	 */
	private String name;
	/**
	 * The route where all locales will be.
	 */
	private static final String ROUTE = "/data/data/com.jkanetwork.st.thiefbusters/data/locales/"; //TODO Nada que hacer, solo tener en cuenta si se cambia el paquete porque aquí es estático
	
	/**
	 * Set a lang for all the game;
	 * 
	 * @param l language selected
	 */
	public static void setLang(String l){
		lang=l;
	}
	
	/**
	 * Create a InterfaceText without a language
	 * 
	 * @param name the name of the file
	 */
	public InterfaceText(String name){
		this.name = name;
		this.loadSentences();
	}
	
	/**
	 * Get the language selected.
	 * 
	 * @return the language in a String
	 */
	@Override
	public String getLang(){		
		return lang;		
	}
	
	
	/**
	 * Get a sentence with the key value. 
	 * 
	 * @param key String with the value which identify the sentence
	 * @return the sentence selected
	 */
	@Override
	public String getSentence(String key) {
		return map.get(key);
	}

	/**
	 * Load all sentences in map.
	 */
	private void loadSentences(){
		try {
			map = Kson.parse(ROUTE+lang+"/"+name);
		} catch (KsonException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Get the number of the sentences loaded. Very useful for debugging.
	 * 
	 * @return the integer with the size
	 */
	@Override
	public int getNumOfSentences() {
		return map.size();
	}
	
	/**
	 * {@inheritDoc}.
	 * 
	 * @return {@inheritDoc}
	 */
	@Override
	public Map<String,String> getMap(){
		return this.map;
	}

}
