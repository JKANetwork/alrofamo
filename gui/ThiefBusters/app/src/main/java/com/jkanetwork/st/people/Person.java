package com.jkanetwork.st.people;

import java.io.Serializable;

/**
 * Superclass for all people.
 * 
 * @version 1.0
 * @since 1.0
 * @author JKA Network
 */
public class Person implements Serializable {
	// Attributes
	/**
	 * Name of each person.
	 */
	private String name;
	/**
	 * Sex of the person, true = female, false = male;
	 */
	private boolean sex;

	// Builders
	/**
	 * Create a Person.
	 * 
	 * @param name
	 *            of player
	 * @param sex
	 *            of player
	 */
	public Person(String name, boolean sex) {
		this.name = name;
		this.sex = sex;
	}

	// Getters
	/**
	 * Get the name.
	 * 
	 * @return name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Get the sex.
	 * 
	 * @return sex
	 */
	public boolean getSex() {
		return this.sex;
	}

	/**
	 * Status of Person.
	 * 
	 * @return Name and sex;
	 */
	@Override
	public String toString() {
		String sex = "M";
		if (this.sex)
			sex = "F";

		return "Name: " + name +", sex: " + sex;
	}
}
