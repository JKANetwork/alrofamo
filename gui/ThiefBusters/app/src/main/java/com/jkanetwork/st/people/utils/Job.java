package com.jkanetwork.st.people.utils;

import com.jkanetwork.st.places.utils.Places;
/**
 * This will determine along with the attribute sex what character have to draw. 
 * Maybe also for drawing will be need nationality and Place.   
 * 
 * @author JKA Network
 */
public enum Job {
	CAPTAIN(Places.AIRPORT), FLIGHT_ATTENDANT(Places.AIRPORT),
	BANKER(Places.BANK), /*POLICE(Places.POLICE),*/ SAILOR(Places.PORT),
	SHIP_CAPTAIN(Places.PORT),STOCKBROKER(Places.STOCK_EXCHANGE), 
	GARDENER(Places.PARK), RELIGIOUS(Places.RELIGIOUS),
	AMBASSOR(Places.EMBASSY), TOUR_GUIDE(Places.MONUMENT),
	SHOPKEEPER(Places.SHOP), LIBRARIAN(Places.LIBRARY),
	MAYOR(Places.TOWN_HALL);
	
	//Attributes
	private Places places;
	
	//Builder
	private Job(Places place){
		this.places = place;
	}
	
	//Getter
	public Places getPlaces(){
		return this.places;
	}
}
