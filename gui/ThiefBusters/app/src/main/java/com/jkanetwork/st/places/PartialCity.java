package com.jkanetwork.st.places;

import com.jkanetwork.st.places.utils.Continent;

import java.io.Serializable;

/**
 * City without destinations and Places.
 * 
 * @since 1.0
 * @version 1.1
 * @author JKA Network
 */
public class PartialCity implements Serializable {
	// Attributes
	/**
	 * Number of the each city.
	 */
	private int id;
	/**
	 * Name of the city.
	 */
	private String name;
	/**
	 * Name of the country.
	 */
	private String country;
	/**
	 * Continent of the country.
	 */
	private Continent continent;
	/**
	 * Description of the city.
	 */
	private String description;

	/**
	 * Places where the player can visit to get proofs.
	 */
	// Builder
	public PartialCity(int id, String name, String country, Continent continent, String description) {
		this.id = id;
		this.name = name;
		this.country = country;
		this.continent = continent;
		this.description = description;
	}

	// Getters
	/**
	 * Get the id.
	 * 
	 * @return id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Get the name.
	 * 
	 * @return name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Get the country.
	 * 
	 * @return country
	 */
	public String getCountry() {
		return this.country;
	}

	/**
	 * Get the continent.
	 * 
	 * @return continent
	 */
	public Continent getContinent() {
		return this.continent;
	}

	/**
	 * Get the description.
	 * 
	 * @return description.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * {@inheritDoc}.
	 * 
	 * @return Status
	 */
	@Override
	public String toString() {
		return "Id: " + id + ", name: " + name + ", country: " + country + ", continent: " + continent
				+ "\ndescription: " + description;
	}
}
