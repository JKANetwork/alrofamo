package com.jkanetwork.st.people.utils;
/**
 * Level of the game.
 * 
 * @author JKA Network
 */

public enum Level {
	EASY,MEDIUM,HARD;
}
