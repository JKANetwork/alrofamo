package com.jkanetwork.st.frontend;

import android.content.Context;

import com.jkanetwork.st.database.GameDatabase;
import com.jkanetwork.st.database.PlayerDatabase;
import com.jkanetwork.st.database.UserInfo;
import com.jkanetwork.st.places.City;
import com.jkanetwork.st.play.Case;

import java.util.Map;

public class Variables {
    public static UserInfo user;
    public static GameDatabase data;
    public static Translatable main;
    public static PlayerDatabase player;
    public static Case caso;
    public static City currentCity;
    public static Map<String,String> VARIANTS;
    public static String PACKAGE;
    public static boolean captured;
    /*public static Context context;*/
    public static boolean exist;
}
