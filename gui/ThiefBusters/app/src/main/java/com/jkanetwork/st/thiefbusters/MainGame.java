package com.jkanetwork.st.thiefbusters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.jkanetwork.st.frontend.InterfaceText;
import com.jkanetwork.st.frontend.Setup;
import com.jkanetwork.st.frontend.Translatable;
import com.jkanetwork.st.frontend.Variables;
import com.jkanetwork.st.places.City;
import com.jkanetwork.st.play.Case;
import com.jkanetwork.st.thiefbusters.methods.Methods;
import com.jkanetwork.st.utils.Utils;

/**
 * Created by joselucross on 19/03/17.
 */

public class MainGame extends AppCompatActivity {
    private TextView text;

    /* El menu */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu); /* El menu está en res/menu/main.xml */
        Methods.menuStart(this,menu);
        return true;
    }
    /* Aquí lo que hacer al dar a cada botón del menu */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.options:
            case R.id.about:
            case R.id.exit:
                return Methods.optionsMenu(item,this);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.getBaseContext().getPackageName();
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        Bundle extra = intent.getExtras();
        setContentView(R.layout.new_game);
        text = (TextView) findViewById(R.id.introduction);
        text.setText("");
        findViewById(R.id.start).setVisibility(View.INVISIBLE);
        if(Variables.currentCity.getId()!=Variables.caso.getCities()[0])
            nextInterface();
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState){
        super.onPostCreate(savedInstanceState);
        String newCase = Variables.main.getSentence("NEWCASE");
        Translatable sex = new InterfaceText("sex.kson");
        String s = sex.getSentence("MALE").toLowerCase();
        int type = Variables.data.getCaseType(Variables.currentCity.getId());
        if(Variables.caso.getThief().getSex())
            s = sex.getSentence("FEMALE").toLowerCase();
        String delito = Variables.data.getCrime(Variables.currentCity.getId(),type);
        newCase = newCase.replace("$thing",delito)
                .replace("$city",Utils.string(Variables.currentCity.getName()))
                .replace("$sex",s).replace("$country",Variables.currentCity.getCountry())
                .replace(
                        "$cargo",
                        Utils.sexVariantsSubstitution(
                                Variables.VARIANTS,
                                new InterfaceText("rank.kson").getSentence(Variables.caso.getPlayer().getRank().name()),
                                Variables.caso.getPlayer().getSex()
                        ).toLowerCase()
                );
        Log.v("Delito",delito);
        if(type==0)
            newCase = newCase.replace("$type",Variables.main.getSentence("STOLE"));
        else
            newCase = newCase.replace("$type",Variables.main.getSentence("HIJACK"));
        newCase = Utils.sexVariantsSubstitution(Variables.VARIANTS,newCase,Variables.caso.getThief().getSex());
        for(char c : newCase.toCharArray()){
            //Utils.delay(100l);
            text.setText(text.getText().toString()+c);
        }
        findViewById(R.id.start).setVisibility(View.VISIBLE);
    }

    public void start(View view){
        nextInterface();
    }

    private void nextInterface(){
        Intent intent = new Intent(this,CityStart.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() { moveTaskToBack(true);}
}
