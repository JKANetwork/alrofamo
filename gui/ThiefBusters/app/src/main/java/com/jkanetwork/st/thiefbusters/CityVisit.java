package com.jkanetwork.st.thiefbusters;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jkanetwork.st.frontend.Variables;
import com.jkanetwork.st.places.City;
import com.jkanetwork.st.thiefbusters.methods.Methods;

/**
 * Created by joselucross on 22/03/17.
 */

public class CityVisit extends AppCompatActivity {

    /* El menu */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu); /* El menu está en res/menu/main.xml */
        Methods.menuStart(this,menu);
        return true;
    }
    /* Aquí lo que hacer al dar a cada botón del menu */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.options:
            case R.id.about:
            case R.id.exit:
                return Methods.optionsMenu(item,this);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.city_visit);
            ((Button) findViewById(R.id.back)).setText(Variables.main.getSentence("BACK"));
            ((TextView) findViewById(R.id.name)).setText(Variables.currentCity.getName());
            places();
        }catch (NullPointerException ex){
            finish();
        }
    }

    private void places(){
        City c = Variables.currentCity;
        ((Button)findViewById(R.id.place1)).setText(c.getPlace(0).getName());
        ((Button)findViewById(R.id.place2)).setText(c.getPlace(1).getName());
        ((Button)findViewById(R.id.place3)).setText(c.getPlace(2).getName());
    }

    public void place1(View view){
        goPlace(0);
    }

    public void place2(View view){
        goPlace(1);
    }

    public void place3(View view){
        goPlace(2);
    }

    private void goPlace(int place){
        if(Variables.currentCity.getPlace(place).getWitness().getTestimony().equals("")){
            Variables.captured=true;
            startActivityForResult(new Intent(this,Finish.class),0);
            finish();
        }else {
            Intent intent = new Intent(this, CityClue.class);
            intent.putExtra("place", place);
            startActivity(intent);
        }

    }

    public void back(View view){
        finish();
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        finish();
    }*/

}
