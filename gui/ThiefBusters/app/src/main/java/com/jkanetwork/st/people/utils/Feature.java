package com.jkanetwork.st.people.utils;
/**
 * Features that the Thief can have.
 * 
 * @author JKA Network
 */
public enum Feature {
	TATTO, EARRING, HAT, CAP, NECKLACE;
}
