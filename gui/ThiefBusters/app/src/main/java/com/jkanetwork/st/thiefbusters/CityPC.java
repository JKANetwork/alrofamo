package com.jkanetwork.st.thiefbusters;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.jkanetwork.st.frontend.InterfaceText;
import com.jkanetwork.st.frontend.Translatable;
import com.jkanetwork.st.frontend.Variables;
import com.jkanetwork.st.people.Thief;
import com.jkanetwork.st.people.utils.Car;
import com.jkanetwork.st.people.utils.Feature;
import com.jkanetwork.st.people.utils.Hair;
import com.jkanetwork.st.people.utils.Hobby;
import com.jkanetwork.st.people.utils.Sex;
import com.jkanetwork.st.thiefbusters.methods.Methods;
import com.jkanetwork.st.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joselucross on 24/03/17.
 */

public class CityPC extends AppCompatActivity {

    Spinner sex,hair,hobby,car,feature;


    /* El menu */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu); /* El menu está en res/menu/main.xml */
        Methods.menuStart(this,menu);
        return true;
    }
    /* Aquí lo que hacer al dar a cada botón del menu */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.options:
            case R.id.about:
            case R.id.exit:
                return Methods.optionsMenu(item,this);
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.city_pc);
            sex = (Spinner) findViewById(R.id.sexSpinner);
            addSpinner(new InterfaceText("sex.kson"), Sex.values(), sex);
            hair = (Spinner) findViewById(R.id.hairSpinner);
            addSpinner(new InterfaceText("hair.kson"), Hair.values(), hair);
            hobby = (Spinner) findViewById(R.id.hobbySpinner);
            addSpinner(new InterfaceText("hobby.kson"), Hobby.values(), hobby);
            car = (Spinner) findViewById(R.id.carSpinner);
            addSpinner(new InterfaceText("car.kson"), Car.values(), car);
            feature = (Spinner) findViewById(R.id.featureSpinner);
            addSpinner(new InterfaceText("feature.kson"), Feature.values(), feature);
            ((Button) findViewById(R.id.back)).setText(Variables.main.getSentence("BACK"));
            ((Button) findViewById(R.id.search)).setText(Variables.main.getSentence("SEARCH"));
            ((TextView) findViewById(R.id.sexpc)).setText(Variables.main.getSentence("SEXPC"));
            ((TextView) findViewById(R.id.hairpc)).setText(Variables.main.getSentence("HAIR"));
            ((TextView) findViewById(R.id.hobbypc)).setText(Variables.main.getSentence("HOBBY"));
            ((TextView) findViewById(R.id.carpc)).setText(Variables.main.getSentence("CAR"));
            ((TextView) findViewById(R.id.featurepc)).setText(Variables.main.getSentence("FEATURE"));
            loadPC();
            setHandlers();
        }catch (NullPointerException ex){
            finish();
        }
    }

    private void loadPC(){
        //SEX
        int position=0;
        {
            if(Variables.caso.getPC().sex == null)
                position = 0;
            else if(Variables.caso.getPC().sex.equals("F"))
                position = 1;
            else if(Variables.caso.getPC().sex.equals("M"))
                position = 2;
            sex.setSelection(position);
        }
        //HAIR
        {
            if(Variables.caso.getPC().hair==null)
                position = 0;
            else
                position = Utils.index(Hair.valueOf(Variables.caso.getPC().hair),Hair.values())+1;
            hair.setSelection(position);
        }
        //HOBBY
        {
            if(Variables.caso.getPC().hobby==null)
                position = 0;
            else
                position = Utils.index(Hobby.valueOf(Variables.caso.getPC().hobby),Hobby.values())+1;
            hobby.setSelection(position);
        }
        //FEATURE
        {
            if(Variables.caso.getPC().feature==null)
                position = 0;
            else
                position = Utils.index(Feature.valueOf(Variables.caso.getPC().feature),Feature.values())+1;
            feature.setSelection(position);
        }
        //CAR
        {
            if(Variables.caso.getPC().car==null)
                position = 0;
            else
                position = Utils.index(Car.valueOf(Variables.caso.getPC().car),Car.values())+1;
            car.setSelection(position);
        }

    }

    private void setHandlers(){
        sex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0)
                    Variables.caso.getPC().sex=null;
                else {
                    String s = "F";
                    if(position==2)
                        s="M";
                    Variables.caso.getPC().sex = s;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        hair.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0)
                    Variables.caso.getPC().hair=null;
                else
                    Variables.caso.getPC().hair=Hair.values()[position-1].name();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        feature.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0)
                    Variables.caso.getPC().feature=null;
                else
                    Variables.caso.getPC().feature=Feature.values()[position-1].name();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        hobby.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0)
                    Variables.caso.getPC().hobby=null;
                else
                    Variables.caso.getPC().hobby=Hobby.values()[position-1].name();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        car.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0)
                    Variables.caso.getPC().car=null;
                else
                    Variables.caso.getPC().car=Car.values()[position-1].name();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void addSpinner(Translatable lang,Enum[] values,Spinner spinner){
        List<String> list = new ArrayList<>(lang.getNumOfSentences()+1);
        list.add("");
        for(int i = 0; i<values.length;i++){
            list.add(lang.getSentence(values[i].name()));
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,list);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);

    }

    public void back(View view){
        finish();
    }

    public void search(View view){
        Log.v("PC Status",Variables.caso.getPC().toString());
        List<Thief> list = Variables.caso.getPC().search();
        TextView text = (TextView) findViewById(R.id.textPC);
        text.setText("");
        text.setMovementMethod(new ScrollingMovementMethod());
        if(list.size()==0){
            text.setText(Variables.main.getSentence("NOTTHIEF"));
        }
        else if(list.size()==1){
            text.setText(Variables.main.getSentence("GETORDER")+": ");
            Variables.caso.setWarrant(list.get(0));
        }
        else
            text.setText(Variables.main.getSentence("MANYTHIEF")+"\n");
        for(Thief thief : list){
            Log.v("EEEOOO","Paso por aquí");
            text.setText(text.getText()+thief.getName()+"\n\t\t\t\t\t\t"+Variables.main.getSentence("HAIR")+": "+(new InterfaceText("hair.kson")).getSentence(thief.getHair().name())+"\n\t\t\t\t\t\t");
            text.setText(text.getText()+Variables.main.getSentence("CAR")+": "+(new InterfaceText("car.kson")).getSentence(thief.getCar().name())+"\n\t\t\t\t\t\t");
            text.setText(text.getText()+Variables.main.getSentence("FEATURE")+": "+(new InterfaceText("feature.kson")).getSentence(thief.getFeature().name())+"\n\t\t\t\t\t\t");
            text.setText(text.getText()+Variables.main.getSentence("HOBBY")+": "+(new InterfaceText("hobby.kson")).getSentence(thief.getHobby().name())+"\n");
        }
    }


}
