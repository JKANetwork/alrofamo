package com.jkanetwork.st.database;

import java.io.IOException;
import java.sql.*;

import com.jkanetwork.st.frontend.Setup;
import com.jkanetwork.st.people.*;
import com.jkanetwork.st.people.utils.Job;
import com.jkanetwork.st.places.*;
import com.jkanetwork.st.places.utils.*;

/**
 * Player database with its personal data.
 * 
 * @author JKA Network
 * @since 1.0
 * @version 1.2
 */
public class PlayerDatabase extends Database {

    private static final String ROUTE="/playerDB.csd";

	// Builder
	/**
	 * Send the route to Database.
	 */
	public PlayerDatabase(String route) throws SQLException {
		super(route+ROUTE);
        connect();
	}

	/**
	 * Return true when the city is saved in player database
	 * 
	 * @param id city id
	 * @return boolean
	 */
	public boolean cityIsSaved(int id){
		try{
			PreparedStatement st = connect.prepareStatement("SELECT * FROM Ciudadesguardadas WHERE Idc=?");
			st.setInt(1, id);
			ResultSet r = st.executeQuery();
			if(r.next())
				return true;
			else
				return false;
		}catch(SQLException ex){
			throw new RuntimeException(ex);
		}
	}
	
	/**
	 * Clean saved games.
	 */
	public void deleteData(){
        try {
            Setup.playerDB();
        }catch(IOException ex){
            throw new RuntimeException(ex);
        }
	}	

	/**
	 * Load a city from saved game.
	 * 
	 * @param id identity of the city to load
	 * @param data GamdeDatabase to load static information of the city
	 * @return City loaded
	 */
	public City loadCity(int id, GameDatabase data) {
		int[] destinations;
		Place[] place;
		int value1 = 0, value2 = 0;
		ResultSet result = null;
		try {
			PreparedStatement st = connect.prepareStatement("SELECT * FROM Ciudadesguardadas WHERE Idc=?");
			st.setInt(1, id);
			result = st.executeQuery();
			while (result.next()) {
				id = result.getInt("IDC");
				value1 = result.getInt("destinosNum");
				value2 = result.getInt("lugaresNum");
			}
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		destinations = new int[value1];
		place = new Place[value2];

		PartialCity pseudocity = data.getPartialCity(id);

		String statement = "SELECT IDC";
		for (int i = 1; i <= destinations.length; ++i) {
			statement += ",destino" + i;
		}
		statement += " FROM Destinos WHERE IDC=" + id;

		try {
			PreparedStatement st = connect.prepareStatement(statement);
			result = st.executeQuery();
			while (result.next()) {
				for (int i = 0; i < destinations.length; ++i) {
					destinations[i] = result.getInt(i + 2);
				}
			}
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		for (int i = 0; i < place.length; ++i) {
			statement = "SELECT idc,idl,nombre,testigoNombre,testigoTrabajo,testigoSexo,testigoTestimonio FROM Lugar WHERE Idc=? AND Idl=?";
			try {
				PreparedStatement st = connect.prepareStatement(statement);
				st.setInt(1, id);
				st.setInt(2, i);
				result = st.executeQuery();
				result.next();
				boolean sex = false;
				if (result.getString("testigoSexo").equals("F"))
					sex = true;
				Witness temp = new Witness(result.getString("testigoNombre"), sex,
						Job.valueOf(result.getString("testigoTrabajo")), result.getString("testigoTestimonio"));
				place[i] = new Place(id,temp,data.getPlaceName(temp.getJob().getPlaces(),id));
				temp.setPlace(place[i]);
			} catch (SQLException ex) {
				throw new RuntimeException(ex);
			}
		}

		return new City(id,pseudocity.getName(),pseudocity.getCountry(),pseudocity.getContinent(),pseudocity.getDescription(),place,destinations);
	}

	/**
	 * Input in a savedGame a city generated.
	 * 
	 * @param city City to save
	 */
	public void saveCity(City city) {
		PreparedStatement st = null;;
		String statement = null;
		try {
			statement = "INSERT INTO CiudadesGuardadas VALUES (?,?,?)";
			st = connect.prepareStatement(statement);
			st.setInt(1, city.getId());
			st.setInt(2, city.getNumOfDestinations());
			st.setInt(3, city.getNumOfPlaces());
			st.execute();
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}//Input in table ciudadesGuardadas
		
		try {
			statement = "INSERT INTO Destinos (IDC";
			for (int i = 1; i <= city.getNumOfDestinations(); ++i) {
				statement += ",destino" + i;
			}
			statement += ") VALUES (?,?";
			for (int i = 1; i < city.getNumOfDestinations(); ++i) {
				statement += ",?";
			}
			statement += ")";
			st = connect.prepareStatement(statement);
			st.setInt(1, city.getId());
			for (int i = 2; i <= city.getNumOfDestinations() + 1; ++i) {
				st.setInt(i, city.getDestination(i - 2));
			}
			st.execute();
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}//Input in table destinos
		
		String sex = "M";
		for (int i = 0; i < city.getNumOfPlaces(); ++i) {
			try {
				statement = "INSERT INTO Lugar (idc,idl,nombre,testigoNombre,testigoTrabajo,testigoSexo,testigoTestimonio) VALUES (?,?,?,?,?,?,?)";
				st = connect.prepareStatement(statement);
				st.setInt(1, city.getId());
				st.setInt(2, i);
				st.setString(3, city.getPlace(i).getName());
				st.setString(4, city.getPlace(i).getWitness().getName());
				st.setString(5, city.getPlace(i).getWitness().getJob().name());
				if (city.getPlace(i).getWitness().getSex())
					sex = "F";
				st.setString(6, sex);
				sex = "M";
				st.setString(7, city.getPlace(i).getWitness().getTestimony());
				st.execute();
			} catch (SQLException ex) {
				throw new RuntimeException(ex);
			}
		}
	}
}
