package com.jkanetwork.st.thiefbusters.methods;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.jkanetwork.st.frontend.Setup;
import com.jkanetwork.st.frontend.Variables;
import com.jkanetwork.st.thiefbusters.About;
import com.jkanetwork.st.thiefbusters.CityTravel;
import com.jkanetwork.st.thiefbusters.CityVisit;
import com.jkanetwork.st.thiefbusters.CityPC;
import com.jkanetwork.st.thiefbusters.Main;
import com.jkanetwork.st.thiefbusters.Options;
import com.jkanetwork.st.thiefbusters.R;

/**
 * Created by joselucross on 21/03/17.
 */

public class Methods {

    public static void travelClick(View view, AppCompatActivity app) {
        Intent intent = new Intent(app, CityTravel.class);
        app.startActivityForResult(intent, 2);
    }

    public static void pcClick(View view, AppCompatActivity app) {
        Intent intent = new Intent(app, CityPC.class); //TODO this works
        app.startActivityForResult(intent, 0);
    }

    public static void visitClick(View view, AppCompatActivity app) {
        Intent intent = new Intent(app, CityVisit.class);
        app.startActivityForResult(intent, 0);
    }

    public static boolean optionsMenu(MenuItem item, AppCompatActivity app) {
        switch (item.getItemId()) {
            case R.id.options:
                Intent options = new Intent(app, Options.class);
                app.startActivity(options);
                return false;
            case R.id.about:
                Intent newIO = new Intent(app, About.class);
                app.startActivity(newIO);
                Log.v("Pasa por aquí", "EEEEEOOOOO");
                return true;
            case R.id.exit:
                int warrantId;
                try {
                    warrantId = Variables.caso.getWarrant().getId();
                } catch (NullPointerException ex) {
                    warrantId = -1;
                }
                Variables.user.save(Variables.caso.getPC(), Variables.currentCity.getId(), Variables.caso.getThief().getId(), warrantId, Variables.caso.getCities());
                app.startActivityForResult(new Intent(app, Main.class), 1);
                app.finish();
                return true;
        }
        return false;
    }

    public static void menuStart(AppCompatActivity app, Menu menu){
        Setup.setMenuText(menu);
        if(app instanceof Main) {
            menu.findItem(R.id.exit).setVisible(false);
        }
    }
}
