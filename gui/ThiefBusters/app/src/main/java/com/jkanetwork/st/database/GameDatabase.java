package com.jkanetwork.st.database;

import android.database.*;
import android.util.Log;

import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.jkanetwork.st.frontend.InterfaceText;
import com.jkanetwork.st.frontend.Translatable;
import com.jkanetwork.st.people.*;
import com.jkanetwork.st.people.utils.*;
import com.jkanetwork.st.places.*;
import com.jkanetwork.st.places.utils.Continent;
import com.jkanetwork.st.places.utils.PlaceType;
import com.jkanetwork.st.places.utils.Places;

public class GameDatabase extends Database {

	// Builder
	public GameDatabase(String route) {
		super(route);
	}

	/**
	 * Get the number of cities in the game.
	 * 
	 * @return integer with the number of cities
	 */
	public int citiesCount() {

		ResultSet result = null;
		int citiesCount = 0;
		try {
			PreparedStatement st = connect.prepareStatement("SELECT COUNT(Idc) FROM Ciudades");
			result = st.executeQuery();
            result.next();
			citiesCount = result.getInt(1);
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
		return citiesCount;
	}

	public String getPlaceName(Places places, int idCity){
        Translatable names = new InterfaceText("places.kson");
        try{
            ResultSet resultSet;
            PreparedStatement st = connect.prepareStatement("SELECT Nombre FROM NombresLugares WHERE IDC=? AND Tipo=?");
            st.setInt(1,idCity);
            st.setString(2,places.name());
            resultSet = st.executeQuery();
            if(resultSet.next())
                return resultSet.getString("Nombre");
            else
                return names.getSentence(places.name());
        }catch (SQLException ex){
            System.err.println(ex.getMessage());
            ex.printStackTrace();
            return names.getSentence(places.name());
        }
    }

    /**
     * Select a city name.
     *
     * @param idcity city that you want to know the name
     * @return String with the city's name
     */
	public String getCityName(int idcity){
        ResultSet result = null;
        try{
            PreparedStatement st = connect.prepareStatement("SELECT Nombre FROM Ciudades WHERE IDC="+idcity);
            result = st.executeQuery();
            result.next();
            return result.getString(1);
        }catch (SQLException ex){
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
        return "ERROR";
    }

	/**
	 * Select a city and set placeNum and destinationNum.
	 * 
	 * @param idcity
	 *            integer with city identity
	 * @param level
	 *            Level to set placeNum and destinationNum
	 * @return a City without destinations and places but with the information
	 *         to generate them
	 */
	public City selectCity(int idcity, Level level) {

		ResultSet result = null;
		String name = null, country = null, description = null;
		Continent continent = null;
		int placeNum = 0, destinationNum = 0;

		switch (level) {
		case EASY:
			placeNum = 3;// Airport/Port, 2 normal
			destinationNum = 3;
			break;
		case MEDIUM:
			placeNum = 3;
			destinationNum = 4;
			break;
		case HARD:
			placeNum = 3;
			destinationNum = 5;
		}

		try {
			PreparedStatement st = connect.prepareStatement("SELECT * FROM Ciudades WHERE Idc=?");
			st.setInt(1, idcity);
			result = st.executeQuery();
			while (result.next()) {
				name = result.getString("Nombre");
				country = result.getString("Pais");
				continent = Continent.valueOf((result.getString("Continente")));
				description = result.getString("Descripcion");
			}
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
		City city = new City(idcity, name, country, continent, description, placeNum, destinationNum);
		return city;
	}

	public boolean havePort(int idcity){
		ResultSet result;
		int port=0;
		try {
			PreparedStatement st = connect.prepareStatement("SELECT Puerto FROM Ciudades WHERE IDC="+idcity);
			result = st.executeQuery();
			if(result.next())
			    port = result.getInt(1);
			if(port==0){
				return false;
			}else{
				return true;
			}
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
		return false;
	}

	/**
	 * Select a random thief to the case.
	 * 
	 * @return thief selected
	 */
	public Thief selectThief() {

		ResultSet result = null;
		Random ranNum = new Random();
		int thiefCount = 0;
		try {
			PreparedStatement st = connect.prepareStatement("SELECT COUNT(Ids) FROM Sospechosos");
			result = st.executeQuery();
            result.next();
			thiefCount = result.getInt(1);
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
		int idThief = (int) (ranNum.nextDouble() * thiefCount + 1);

        return selectThief(idThief);
		
	}
	
	/**
	 * Select a thief using the number passed.
	 * 
	 * @param id id of the thief
	 * @return thief selected
	 */
	public Thief selectThief(int id){
		
		ResultSet result = null;
		String name = null;
		Hair hair = null;
		Car car = null;
		Feature feature = null;
		Hobby hobby = null;
		boolean sex = false;

		
		try {
			PreparedStatement st = connect.prepareStatement("SELECT * FROM Sospechosos WHERE Ids=?");
			st.setInt(1, id);
			result = st.executeQuery();
			while (result.next()) {
				name = result.getString("Nombre");
				if (result.getInt("Sexo") == 1)
					sex = true;
				hair = Hair.valueOf((result.getString("Pelo")));
				car = Car.valueOf((result.getString("Coche")));
				feature = Feature.valueOf((result.getString("Caracteristica")));
				hobby = Hobby.valueOf((result.getString("Hobby")));
			}
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
		Thief thief = new Thief(id, name, sex, hair, car, feature, hobby);
		return thief;		

	}

	/**
	 * Get a clue to give to player.
	 * 
	 * @param idcity
	 *            City identification to select clue
	 * @param level
	 *            Level of the clue
	 * @param type
	 *            PlaceType of the clue
	 * @return string with the clue
	 */
	public String clueCity(int idcity, Level level, PlaceType type) {
		double random = Math.random();
		Level clueLevel = null;
		ResultSet result;
		Random rand = new Random();

		switch (level) {
		case EASY:
			if (random < 0.8)
				clueLevel = Level.EASY;
			else
				clueLevel = Level.MEDIUM;
			break;
		case MEDIUM:
			if (random < 0.4)
				clueLevel = Level.EASY;
			else if (random < 0.8)
				clueLevel = Level.MEDIUM;
			else
				clueLevel = Level.HARD;
			break;
		case HARD:
			if (random < 0.2)
				clueLevel = Level.EASY;
			else if (random < 0.5)
				clueLevel = Level.MEDIUM;
			else
				clueLevel = Level.HARD;
			break;
		}

		if(type==PlaceType.TRAVEL)
		    clueLevel=Level.EASY;

		try {
			PreparedStatement st = connect
					.prepareStatement("SELECT COUNT(Idp) FROM PistasCiudad WHERE Tipo=? AND Nivel=? AND Idc=?");
			st.setString(1, type.name());
			st.setString(2, clueLevel.name());
			st.setInt(3, idcity);
			result = st.executeQuery();

			/* Go to result before using it */
			result.first();
			int value = (int) (rand.nextDouble() * result.getInt(1) + 1);
			int contador = 1;
			st = connect.prepareStatement("SELECT Pista FROM PistasCiudad WHERE Tipo=? AND Nivel=? AND Idc=?");
			st.setString(1, type.name());
			st.setString(2, clueLevel.name());
			st.setInt(3, idcity);
            result = st.executeQuery();
			while (result.next()) {
				if (value == contador++) {
					Log.d("Se encontró","Value:"+value+" result:" +result.getString(1));
                    if(result.getString(1)!=null)
					    return result.getString(1);
                    else
                        return "Clue not added, the city is: "+this.getCityName(idcity);
				}
			}
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}

		return "Clue not added, the city is: "+this.getCityName(idcity)+".";
	}

	/**
	 * Get a clue to give to player.
	 * 
	 * @param level
	 *            Level of the clue
	 * @param thief
	 *            thief to get features to get a clue
	 * @return string with the clue
	 */
	public String clueThief(Level level, Thief thief) {
		Random rand = new Random();
		int num = (int) (rand.nextDouble() * 4 + 1);
		String search;
		String searchValue;
		ResultSet result;

		switch (num) {
		case 1:
			search = "HAIR";
			searchValue = thief.getHair().name();
			break;
		case 2:
			search = "CAR";
			searchValue = thief.getCar().name();
			break;
		case 3:
			search = "FEATURE";
			searchValue = thief.getFeature().name();
			break;
		default:
			search = "HOBBY";
			searchValue = thief.getHobby().name();
			break;
		}

		try {
			PreparedStatement st = connect
					.prepareStatement("SELECT Pista FROM PistasSospechosos WHERE Tipo=? AND Nivel=? AND ValorTipo=?");
			st.setString(1, search);
			st.setString(2, level.name());
			st.setString(3, searchValue);
			result = st.executeQuery();
			if(result.next())
				return result.getString(1);
			else
				return "";
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}

		return "";
	}

	/**
	 * Get a partialCity to load city in PlayerDatabase.
	 * 
	 * @param id
	 *            id of the city to select
	 * @return a PartialCity
	 */
	protected PartialCity getPartialCity(int id) {
		ResultSet result;
		try {
			PreparedStatement st = connect.prepareStatement("SELECT * FROM Ciudades WHERE IDC=?");
			st.setInt(1, id);
			result = st.executeQuery();

			result.next();
			return new PartialCity(id, result.getString("Nombre"), result.getString("Pais"),
					Continent.valueOf(result.getString("Continente")), result.getString("Descripcion"));
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
		return null;
	}

	public List<Thief> getThiefByPC(String query) {
		ResultSet result;
		List<Thief> thieves = new ArrayList<>();
		try {
			PreparedStatement st = connect.prepareStatement(query);
			result = st.executeQuery();
			while (result.next()) {

				boolean sex = false;
				if (result.getInt(3)==1)
					sex = true;
				thieves.add(new Thief(result.getInt(1),
						result.getString(2),
						sex,
						Hair.valueOf(result.getString(4)),
						Car.valueOf(result.getString(5)),
						Feature.valueOf(result.getString(6)),
						Hobby.valueOf(result.getString(7))));
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
            e.printStackTrace();
			return new ArrayList<Thief>();
		}
		return thieves;

	}

	public int getCaseType(int idCity){
        try{
            PreparedStatement pt = connect.prepareStatement("SELECT Tipo FROM Delitos WHERE IDC=?");
            pt.setInt(1,idCity);
            ResultSet resultSet = pt.executeQuery();
			LinkedList<Integer> integer = new LinkedList<>();
            while (resultSet.next()){
                integer.add(resultSet.getInt("Tipo"));
            }
			Collections.shuffle(integer);
			return integer.get(0);
        }catch (SQLException ex){
            return 0;
        }catch (IndexOutOfBoundsException ex){
			return 0;
		}
    }

    public String getCrime(int idCity,int type){
        try{
            PreparedStatement pt = connect.prepareStatement("SELECT * FROM Delitos WHERE IDC=? AND Tipo=?");
            pt.setInt(1,idCity);
            pt.setInt(2,type);
            ResultSet resultSet = pt.executeQuery();
            LinkedList<String> crimes = new LinkedList<>();
            while (resultSet.next()){
                crimes.add(resultSet.getString("Delito"));
            }
			return crimes.get(0);
        }catch (SQLException ex){
			Log.e("ERROR","Excepción lanzada",ex);
            return "algo";
        }catch (IndexOutOfBoundsException ex){
			return "algo";
		}
    }
}
