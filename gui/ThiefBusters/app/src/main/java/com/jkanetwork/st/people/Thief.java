package com.jkanetwork.st.people;

import com.jkanetwork.st.people.utils.*;

import java.util.Objects;

/**
 * Person who steal and is pursued by player.
 * 
 * @version 1.0;
 * @since 1.0;
 * @author JKA Network
 */
public class Thief extends Person {
	
	//Attributes
	/**
	 * Database identification
	 */
	private int id;
	/**
	 * Thief's hair.
	 */
	private Hair hair;
	/**
	 * Thief's car.
	 */
	private Car car;
	/**
	 * Thief's feature.
	 */
	private Feature feature;
	/**
	 * Thief's hobby.
	 */
	private Hobby hobby;
	
	//Builders
	/**
	 * Create a Thief.
	 * 
	 * @param id
	 * @param name
	 * @param sex
	 * @param hair
	 * @param car
	 * @param feature
	 * @param hobby
	 */
	public Thief(int id,String name, boolean sex, Hair hair, Car car, Feature feature, Hobby hobby){
		super(name, sex);
		this.id = id;
		this.hair = hair;
		this.car = car;
		this.feature = feature;
		this.hobby = hobby;
	}
	
	//Getters
	/**
	 * Get a database identification of thief
	 *
	 * @return integer with the id
	 */
	public int getId(){
		return this.id;
	}
	/**
	 * Get the hair
	 * 
	 * @return hair
	 */
	public Hair getHair(){
		return this.hair;
	}
	/**
	 * Get the car
	 * 
	 * @return car
	 */
	public Car getCar(){
		return this.car;
	}
	/**
	 * Get he feature
	 * 
	 * @return feature
	 */
	public Feature getFeature(){
		return this.feature;
	}
	/**
	 * Get the hobby
	 * 
	 * @return hobby
	 */
	public Hobby getHobby(){
		return this.hobby;
	}
	/**
	 * {@inheritDoc}.
	 * 
	 * @return {@inheritDoc}
	 */
	@Override
	public String toString(){
		return super.toString() + ". Hair: " + hair + ", car: " + car + ", feature: " + feature + ", hobby: " + hobby;
	}

	@Override
	public boolean equals(Object o){
		if(o instanceof Thief)
			if(((Thief) o).getId()==this.getId())
				return true;
		return false;
	}
}
