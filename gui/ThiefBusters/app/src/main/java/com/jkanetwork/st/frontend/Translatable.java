package com.jkanetwork.st.frontend;

import java.util.Locale;
import java.util.Map;

/**
 * Interface to provide a contract for every class will be have 
 * different text according the LANG. 
 * 
 * @author JKA Network
 */
public interface Translatable {
	
	/**
	 * The LANG (or lang where not final) must be in lowercase
	 */
	final static String LANG= Locale.getDefault().getLanguage();
	
	/**
	 * Select a string in the array with the index i.
	 *
	 * @return the String with the sentence
	 */
	String getSentence(String key);
	
	/**
	 * Give the language. Will be necessary to open files with the translations.
	 * 
	 * @return language
	 */
	String getLang();
	
	/**
	 * Get the number of sentences
	 * 
	 * @return Array's String length
	 */
	int getNumOfSentences();
	
	/**
	 * Get the map with the translations
	 * 
	 * @return the map
	 */
	Map<String,String> getMap();
	
}
