package com.jkanetwork.st.places.utils;

/**
 * Places where player will able to visit in game.
 * 
 * @author JKA Network
 */
public enum Places {
	//Cultural places
	LIBRARY(PlaceType.CULTURAL),RELIGIOUS(PlaceType.CULTURAL),MONUMENT(PlaceType.CULTURAL),
	//Political places
	EMBASSY(PlaceType.POLITICAL),TOWN_HALL(PlaceType.POLITICAL),PARK(PlaceType.POLITICAL),
	//Commercial places
	BANK(PlaceType.COMMERCIAL),STOCK_EXCHANGE(PlaceType.COMMERCIAL),SHOP(PlaceType.COMMERCIAL),
	//Travel places
	PORT(PlaceType.TRAVEL),AIRPORT(PlaceType.TRAVEL)
	//Police
	/*POLICE(PlaceType.POLICE)*/;
	
	//Attributes
	/**
	 * type of place
	 */
	private PlaceType type;
	
	//Builder
	/**
	 * Give to Places its PlaceType.
	 * 
	 * @param type
	 */
	private Places(PlaceType type){
		this.type = type;
	}
	
	//Getter
	/**
	 * Get a placeType.
	 * 
	 * @return a type
	 */
	public PlaceType getPlaceType(){
		return this.type;
	}
	
}
