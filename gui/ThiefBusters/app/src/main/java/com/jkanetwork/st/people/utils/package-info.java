/**
 * Utilities for people definitions.
 * 
 * @author JKA Network
 */
package com.jkanetwork.st.people.utils;