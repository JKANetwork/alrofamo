package com.jkanetwork.st.database;

import android.content.Context;
import android.provider.ContactsContract;

import com.jkanetwork.st.frontend.Variables;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Class which connect the database.
 * 
 * @author JKA Network
 */
public abstract class Database implements Serializable {
	// Attributes
	/**
	 * Path of database.
	 */
	protected String route;
	/**
	 * Connection with database.
	 */
	transient protected Connection connect;

	// Builder
	/**
	 * Give the Connector class a route.
	 *
	 * @param route
	 */
	public Database(String route) {
		this.route = route;
	}

	// The next code is from
	// http://serprogramador.es/como-conectar-y-utilizar-java-con-sqlite/
	/**
	 * Connect the database.
	 * @throws SQLException 
	 */
	public void connect() throws SQLException {
        try {
                DriverManager.registerDriver((Driver) Class.forName("org.sqldroid.SQLDroidDriver").newInstance());
        }catch(Exception ex) {
            throw new RuntimeException(ex);
        }

        this.connect = DriverManager.getConnection("jdbc:sqldroid:"+"/data/data/"+ this.route);
	}

	/**
	 * Close the database.
	 */
	public void close() {
        try {
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isConnect(){
		try{
			return !connect.isClosed();
		}catch (SQLException ex){
			return false;
		}

	}

}
