package com.jkanetwork.st.places;

import com.jkanetwork.st.people.Witness;
import com.jkanetwork.st.places.utils.Places;

import java.io.Serializable;

/**
 * Locations where the player could visit and obtain testimonies (or not).
 * 
 * @version 1.0
 * @since 1.0
 * @author JKANetwork
 */
public class Place implements Serializable {
	// Attributes
	/**
	 * City where the place is.
	 */
	private int idCity; //Probably to remove
	/**
	 * Witness who give testimonies in this place
	 */
	private Witness witness;
	/**
	 * Type of Place linked with the type of testimony.
	 */
	private Places place;
	/**
	 * Name of this place
	 */
	private String name;
	
	// Builder
	/**
	 * Build the place
	 * 
	 * @param city
	 * @param witness
	 */
	public Place(int city, Places place, Witness witness, String name) {
		this.idCity = city;
		this.place = place;
		this.witness = witness;
		this.name = name;
	}
	
	/**
	 * Build the place when is load from user database
	 * 
	 * @param city
	 * @param witness
	 * @param name
	 */
	public Place(int city,Witness witness, String name){
		this.idCity = city;
		this.witness = witness;
		this.name = name;
		this.place = this.witness.getJob().getPlaces();
	}
	
	//Setters

	// Getters
	/**
	 * City linked of this city.
	 * 
	 * @return city
	 */
	public int getIdCity() {
		return this.idCity;
	}

	/**
	 * Witness of this place.
	 * 
	 * @return witness
	 */
	public Witness getWitness() {
		return this.witness;
	}

	/**
	 * Get the place type.
	 * 
	 * @return type
	 */
	public Places getPlaces() {
		return this.place;
	}

	/**
	 * Get the name.
	 * 
	 * @return name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * {@inheritDoc}.
	 * 
	 * @return {@inheritDoc}
	 */
	@Override
	public String toString(){
		return "Type: " + place + ", witness: " + witness;
	}
}
