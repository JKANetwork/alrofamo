package com.jkanetwork.st.people.utils;
/**
 * Type of Car that thief drive.
 * 
 * @author JKA Network
 */
public enum Car {
	FORD, MERCEDES, OPEL, FIAT, VOLKSWAGEN;
}
