package com.jkanetwork.st.people.utils;
/**
 * Hair color that the thief may have.
 * 
 * @author JKA Network
 */
public enum Hair {
	BLONDE, RED, BROWN, BLACK;
}
