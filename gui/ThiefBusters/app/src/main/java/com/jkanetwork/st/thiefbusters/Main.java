package com.jkanetwork.st.thiefbusters;


import android.app.DialogFragment;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jkanetwork.st.database.*;
import com.jkanetwork.st.frontend.InterfaceText;
import com.jkanetwork.st.frontend.Setup;
import com.jkanetwork.st.frontend.Translatable;
import com.jkanetwork.st.frontend.Variables;
import com.jkanetwork.st.people.Detective;
import com.jkanetwork.st.people.Thief;
import com.jkanetwork.st.people.utils.Hair;
import com.jkanetwork.st.people.utils.Rank;
import com.jkanetwork.st.places.City;
import com.jkanetwork.st.play.Case;
import com.jkanetwork.st.play.PC;
import com.jkanetwork.st.thiefbusters.dialogs.NewUserDialog;
import com.jkanetwork.st.thiefbusters.methods.Methods;
import com.jkanetwork.st.utils.Utils;

import java.util.Map;


public class Main extends AppCompatActivity {

    private Detective detective;
    private Case caso;
    private Bundle bundle;
    private PC pc;
    Button start, load;

    /* El menu */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu); /* El menu está en res/menu/main.xml */
        Methods.menuStart(this,menu);
        return true;
    }
    /* Aquí lo que hacer al dar a cada botón del menu */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.options:
            case R.id.about:
            case R.id.exit:
                return Methods.optionsMenu(item,this);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*public void testAll(){
        TextView text = (TextView) findViewById(R.id.firstText);
        try {
            UserInfo userinfo = new UserInfo(getPackageName()+"/user.kson");;
            try {
                userinfo.checkFile();
            }catch(BadUserInfoFileException ex){
                userinfo.create(new Detective("José Luis",false, Rank.RECRUIT,0));
            }
            text.setText("Usuario KSON conectado. Ejemplo ->" + userinfo.loadDetective());
            if(!Setup.checkVersion(getPackageName(),getPackageManager().getPackageInfo(getPackageName(),0).versionName))
                Setup.setup(getAssets(),getPackageName(),true);
            else
                Setup.setup(getAssets(),getPackageName());
            Database user = new PlayerDatabase(getPackageName());
            ((PlayerDatabase) user).deleteData();
            text.setText(text.getText()+"\nBase de datos de usuario conectada");
            InterfaceText interfaz = new InterfaceText("hair.kson");
            text.setText(text.getText()+"\nLocales correctos. Ejemplo: "+ Hair.BLACK.name() +"->" + interfaz.getSentence(Hair.BLACK.name()));
            GameDatabase data = new GameDatabase(getPackageName()+"/data/gamedata/data.csd");
            data.connect();
            text.setText(text.getText()+"\nBase de datos del juego correcta: Ladrón al azar->" + data.selectThief());
            text.setText(text.getText()+"\nTodo correcto");
            user.close();
            data.close();
        }catch (Exception ex){
            text.setText(text.getText()+"\nError en las conexiones\n" + ex.getMessage() + ex.getLocalizedMessage());
            ex.printStackTrace();
        }
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        bundle = savedInstanceState;
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            //testAll();
            start();
            pc = new PC(Variables.data);
            start = (Button) findViewById(R.id.newGame);
            load = (Button) findViewById(R.id.continueGame);
        }catch (Exception e){
            e.printStackTrace();
            TextView text = (TextView) findViewById(R.id.firstText);
            text.setText(e.getMessage()+e.getLocalizedMessage());
        }
        City.currentCity=0;
        Variables.exist=true;
    }

    private void start(){
        try{
            //Variables.context = this;
            //Check instalation
            if(!Setup.checkVersion(getPackageName(),getPackageManager().getPackageInfo(getPackageName(),0).versionName))
                Setup.setup(getAssets(),getPackageName(),true);
            else
                Setup.setup(getAssets(),getPackageName());
            Setup.startVariables(this);
            findViewById(R.id.newGame).setEnabled(true);
            findViewById(R.id.loading).setVisibility(View.INVISIBLE);
            findViewById(R.id.loadingText).setVisibility(View.INVISIBLE);
            ((Button) findViewById(R.id.newGame)).setText(Variables.main.getSentence("NEW"));
            ((Button) findViewById(R.id.continueGame)).setText(Variables.main.getSentence("CONTINUE"));

        }catch(Exception ex){
            throw new RuntimeException(ex);
        }
        boolean flag=true;
        try {
            Setup.reloadUserInfo(this);
        }catch (BadUserInfoFileException ex){
            getNewUser();
            findViewById(R.id.continueGame).setEnabled(false);
            flag=false;
        }
        if(flag && !Variables.user.isSaved())
            ((Button) findViewById(R.id.continueGame)).setEnabled(false);

    }

    private void getNewUser() {
        DialogFragment d = new NewUserDialog();
        d.setCancelable(false);
        d.show(getFragmentManager(), "NEWUSER");
    }

    public void newCase(View view){
        detective = Variables.user.loadDetective();
        //First delete all data
        Variables.player.deleteData();
        //Then generate a case
        caso = new Case(detective,Variables.data,Variables.VARIANTS,pc);
        //Save the new game
        Variables.user.save(pc,caso.getCities()[0],caso.getThief().getId(),-1,caso.getCities());
        City c= caso.loadCity(caso.getCities()[0],Variables.player,Variables.data,Variables.VARIANTS);
        Variables.player.saveCity(c);
        play(c);
    }

    public void loadCase(View view) {
        detective = Variables.user.loadDetective();
        //First load the thief and thief with warrant
        Thief thief = Variables.user.loadThief(Variables.data);
        Thief warrant = Variables.user.loadThiefPC(Variables.data);
        //Finnaly load all game
        int[] cities = new int[Variables.user.numOfCities()];
        int city = Variables.user.loadGame(pc,cities);
        Log.d("LISTA", Utils.getStringFromArray(cities));
        //Load the case
        caso = new Case(detective,thief,warrant,pc,cities);
        play(caso.loadCity(city,Variables.player,Variables.data,Variables.VARIANTS));
    }

    private void play(City city){
        Intent intent;
        intent = new Intent(this,MainGame.class);
        Variables.captured=false;
        Variables.currentCity=city;
        Variables.caso=caso;
        City.currentCity=Variables.currentCity.getId();
        startActivityForResult(intent,0);

        finish();
    }

    @Override
    public void onBackPressed() { moveTaskToBack(true);}
}

