package com.jkanetwork.st.frontend;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Build;
import android.view.Menu;
import android.view.MenuItem;

import com.jkanetwork.st.database.BadUserInfoFileException;
import com.jkanetwork.st.database.GameDatabase;
import com.jkanetwork.st.database.PlayerDatabase;
import com.jkanetwork.st.database.UserInfo;
import com.jkanetwork.st.people.Thief;
import com.jkanetwork.st.places.City;
import com.jkanetwork.st.play.Case;
import com.jkanetwork.st.play.PC;
import com.jkanetwork.st.thiefbusters.R;
import com.jkanetwork.st.utils.Utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.Locale;


public class Setup {

    private static final String[] SUPPORTEDLANGS = {"es", "en"};
    private static AssetManager manager;
    private static String superRoute;
    private static String packag;

    public static void setup(AssetManager man, String pack,boolean newVersion) throws IOException{
        superRoute="/data/data/"+pack;
        packag = pack;
        String db = superRoute+"/data/gamedata/data.csd";
        manager=man;
        if(!checkDatabase(db)||newVersion){
            InputStream stream=manager.open("data/gamedata/data.csd");
            OutputStream writer = new FileOutputStream(new File(db));
            byte[] buffer = new byte[stream.available()];
            stream.read(buffer);
            writer.write(buffer);
            writer.close();
            stream.close();
        }
        String local =superRoute+"/data/locales/";
        if(Utils.index(Locale.getDefault().getLanguage(),SUPPORTEDLANGS)!=-1)
            local += Locale.getDefault().getLanguage();
        else {
            local += "en";
            InterfaceText.setLang("en");
        }
        if(!checkLocales(local)||newVersion){
            String assetPath = "data/locales/"+Locale.getDefault().getLanguage();
            String[] it = manager.list(assetPath);
            for(String s : it){
                File file = new File(local+"/"+s);
                file.createNewFile();
                InputStream stream=manager.open(assetPath+"/"+s);
                OutputStream writer = new FileOutputStream(file);
                byte[] buffer = new byte[stream.available()];
                stream.read(buffer);
                writer.write(buffer);
                writer.close();
                stream.close();
            }
        }
        String db1 = superRoute+"/playerDB.csd";
        if(!checkPlayerDatabase(db1)){
            playerDB();
        }
    }

    public static void playerDB() throws IOException{
        String db1 = superRoute+"/playerDB.csd";
        InputStream stream=manager.open("data/playerdata/playerDB.csd");
        OutputStream writer = new FileOutputStream(new File(db1));
        byte[] buffer = new byte[stream.available()];
        stream.read(buffer);
        writer.write(buffer);
        writer.close();
        stream.close();
    }

    public static boolean checkVersion(String pack,String v) throws IOException{
        File version = new File("/data/data/"+pack+"/versionID");
        if(!version.exists()){
            version.createNewFile();
            BufferedWriter bw = new BufferedWriter(new FileWriter(version));
            bw.write(v);
            bw.close();
            return true;
        }
        BufferedReader br = new BufferedReader(new FileReader(version));
        String value = br.readLine();
        br.close();
        if(v.equals(value))
            return true;
        else {
            BufferedWriter bw = new BufferedWriter(new FileWriter(version));
            bw.write(v);
            bw.close();
            return false;
        }
    }

    public static void setup(AssetManager manager, String pack) throws IOException{
        setup(manager,pack,false);
    }

    private static boolean checkDatabase(String route) throws IOException{
        File file = new File(route);
        if(file.exists())
            return true;
        else{
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
        return false;
    }

    private static boolean checkPlayerDatabase(String route) throws IOException{
        File file = new File(route);
        if(file.exists())
            return true;
        else{
            file.createNewFile();
        }
        return false;
    }

    private static boolean checkLocales(String route){
        File file = new File(route);
        if (file.exists())
            return true;
        else {
            file.mkdirs();
        }
        return false;
    }

    public static void startVariables(Context context) throws SQLException{
        Variables.PACKAGE = context.getPackageName();
        Variables.VARIANTS = (new InterfaceText("gendermorphemes.kson")).getMap();
        Variables.main = new InterfaceText("main.kson");
        Variables.data = new GameDatabase(context.getPackageName()+"/data/gamedata/data.csd");
        Variables.data.connect();
        Variables.player = new PlayerDatabase(context.getPackageName());
        City.incorrect = new InterfaceText("incorrectTestimonies.kson");
    }

    public static void reloadUserInfo(Context context) throws BadUserInfoFileException{
        Variables.user = new UserInfo(context.getPackageName() + "/user.kson");
        Variables.user.checkFile();
    }

    public static Case reloadCase(){
        //First load the thief and thief with warrant
        Thief thief = Variables.user.loadThief(Variables.data);
        Thief warrant = Variables.user.loadThiefPC(Variables.data);
        //Finnaly load all game
        int[] cities = new int[Variables.user.numOfCities()];
        PC swap = new PC(Variables.data);
        Variables.user.loadGame(swap,cities);
        //Load the case
        return new Case(Variables.user.loadDetective(),thief,warrant,swap,cities);
    }

    public static void setMenuText(Menu it){
        Translatable menu = new InterfaceText("menu.kson");
        it.findItem(R.id.options).setTitle(menu.getSentence("OPTIONS"));
        it.findItem(R.id.exit).setTitle(menu.getSentence("EXIT"));
        it.findItem(R.id.about).setTitle(menu.getSentence("ABOUT"));
    }

}
