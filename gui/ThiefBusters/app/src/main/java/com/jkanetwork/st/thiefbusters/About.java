package com.jkanetwork.st.thiefbusters;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

/**
 * Created by joselucross on 10/04/17.
 */

public class About extends AppCompatActivity {

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.about);
        ((WebView) findViewById(R.id.aboutWebView)).loadUrl("http://mirror.jkanetwork.com/SoftwareTalent/ThiefBusters/www/about.html");
    }
}
