package com.jkanetwork.st.places.utils;
/**
 * Type of places in the city where the player could visit.
 * 
 * @author JKA Network
 */
public enum PlaceType {
	CULTURAL, POLITICAL, COMMERCIAL, /*POLICE,*/ TRAVEL;
}
