package com.jkanetwork.st.thiefbusters.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.DialogFragment;
import android.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.jkanetwork.st.database.BadUserInfoFileException;
import com.jkanetwork.st.database.UserInfo;
import com.jkanetwork.st.frontend.InterfaceText;
import com.jkanetwork.st.frontend.Translatable;
import com.jkanetwork.st.frontend.Variables;
import com.jkanetwork.st.people.Detective;
import com.jkanetwork.st.people.utils.Rank;
import com.jkanetwork.st.thiefbusters.Main;
import com.jkanetwork.st.thiefbusters.R;

public class NewUserDialog extends DialogFragment{

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Translatable main = new InterfaceText("main.kson");
        Translatable sex = new InterfaceText("sex.kson");
        final View content = getActivity().getLayoutInflater().inflate(R.layout.dialog_new_user,null);

        TextView text;
        text = (TextView) content.findViewById(R.id.welcome);

        try{
            Variables.user.checkFile();
            text.setText(main.getSentence("CHANGE"));
            ((EditText) content.findViewById(R.id.nameBox)).setText(Variables.user.loadDetective().getName());
            if(!Variables.user.loadDetective().getSex()){
                ((RadioButton) content.findViewById(R.id.boy)).setChecked(true);
                ((RadioButton) content.findViewById(R.id.girl)).setChecked(false);
            }
        }catch (Exception ex){
            text.setText(main.getSentence("WELCOME"));
        }
        text = (TextView) content.findViewById(R.id.name);
        text.setText(main.getSentence("NAME"));
        text = (TextView) content.findViewById(R.id.sex);
        text.setText(main.getSentence("SEX"));
        RadioButton radio;
        radio = (RadioButton) content.findViewById(R.id.girl);
        radio.setText(sex.getSentence("FEMALE"));
        radio = (RadioButton) content.findViewById(R.id.boy);
        radio.setText(sex.getSentence("MALE"));


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setView(content)
                // Add action buttons
                .setPositiveButton(main.getSentence("OK"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        EditText name = (EditText) content.findViewById(R.id.nameBox);
                        String username = name.getText().toString();
                        boolean sex=true;
                        if(((RadioButton) content.findViewById(R.id.boy)).isChecked())
                            sex=false;
                        UserInfo user=null;
                        try{
                            user = new UserInfo(Variables.PACKAGE+"/user.kson");
                            user.checkFile();
                            user.create(new Detective(username,sex,Variables.user.loadDetective().getRank(),Variables.user.loadDetective().getCompleted()));
                        }catch (BadUserInfoFileException ex){
                            user.create(new Detective(username,sex,Rank.RECRUIT,0));
                        }
                        Variables.user = user;
                    }
                });
        return builder.create();
    }

}
