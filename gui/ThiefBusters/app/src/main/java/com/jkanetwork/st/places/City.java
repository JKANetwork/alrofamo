package com.jkanetwork.st.places;

import java.util.Map;
import java.util.Random;

import com.jkanetwork.st.database.*;
import com.jkanetwork.st.frontend.Translatable;
import com.jkanetwork.st.people.Detective;
import com.jkanetwork.st.people.Thief;
import com.jkanetwork.st.people.Witness;
import com.jkanetwork.st.people.utils.Job;
import com.jkanetwork.st.people.utils.Level;
import com.jkanetwork.st.places.utils.*;
import com.jkanetwork.st.thiefbusters.BuildConfig;
import com.jkanetwork.st.utils.*;

/**
 * Class of the city where the player can visit.
 * 
 * @version 2.0
 * @since 1.0
 * @author JKA Network
 */
public class City extends PartialCity {
	// Attributes
	/**
	 * Places where the player can visit in the city
	 */
	private Place[] places;
	/**
	 * Cities where the user can travel.
	 */
	private int[] destinations;
    /**
     *
     */
    private boolean error;

	// Static attributes
	public static Translatable incorrect;
	public static int beforeCity; // Only important in new cities
	public static int currentCity;

	// Builder
	/**
	 * Build the city.
	 * 
	 * @param id
	 *            city's id
	 * @param name
	 *            city's name
	 * @param country
	 *            country's name
	 * @param continent
	 *            country's continent
	 * @param description
	 *            city's description
	 * @param placesNum
	 *            Number of places the player can visit
	 * @param destinationsNum
	 *            Number of destinations will be need
	 */
	public City(int id, String name, String country, Continent continent, String description, int placesNum,
			int destinationsNum) {
		super(id, name, country, continent, description);

		this.destinations = new int[destinationsNum];
		this.places = new Place[placesNum];
		beforeCity = currentCity;
		currentCity = id;
	}

	/**
	 * Load a city.
	 * 
	 * @param id
	 *            city's id
	 * @param name
	 *            city's name
	 * @param country
	 *            country's name
	 * @param continent
	 *            country's continent
	 * @param description
	 *            city's description
	 * @param places
	 * @param destinations
	 */
	public City(int id, String name, String country, Continent continent, String description, Place[] places,
			int[] destinations) {
		super(id, name, country, continent, description);
		this.places = places;
		this.destinations = destinations;
        beforeCity = currentCity;
        currentCity = id;
	}

	/**
	 * Create all destinations.
	 */
	public void destinationsGeneration(GameDatabase data, int[] caseCities) {
		int posibleDestinations = data.citiesCount();
		int temp;
		int cityToBegin = 1;
		Random rand = new Random();
		int actualCityIndex = Utils.indexInt(this.getId(), caseCities);
		if (beforeCity != 0 && Utils.NotInIntArray(beforeCity, caseCities)) {
			destinations[0] = beforeCity;
            this.error=true;
			for (int i = 1; i < destinations.length; ++i) {
				destinations[i] = -1;
			}
		} else {
			if (beforeCity == 0) {
				do {
					temp = (int) (rand.nextDouble() * posibleDestinations + 1);
				} while (!Utils.NotInIntArray(temp, caseCities) || temp == this.getId());
				destinations[0] = temp;
			} else {
				destinations[0] = beforeCity;
			}
			if (actualCityIndex != caseCities.length - 1 && actualCityIndex != -1) {
				destinations[1] = caseCities[actualCityIndex + 1];
				cityToBegin = 2;
			}
			for (int i = cityToBegin; i < destinations.length; ++i) {
				do {
					temp = (int) (rand.nextDouble() * posibleDestinations + 1);
				} while (!Utils.NotInIntArray(temp, caseCities) || !Utils.NotInIntArray(temp, destinations)
						|| temp == this.getId());
				destinations[i] = temp;
			}
		}
		if(!error)
			destinations = Utils.shuffle(destinations);
	}

	/**
	 * Generation of locations and witness.
	 * 
	 * @param caseCities
	 *            correct cities
	 * @param thief
	 *            thief in this case
	 * @param sexVariants
	 *            Map with the variants of sex morphemes to replace strings
	 * @param game
	 *            database with game data
	 * @param player
	 *            user who is playing now
	 */
	public void locationGeneration(int[] caseCities, Thief thief, Map<String, String> sexVariants, GameDatabase game,
			Detective player) {
		PlaceType[] types = new PlaceType[this.getNumOfPlaces()];
		Random rand = new Random();
		Job[] jobs = Job.values();
		int counter = 0;

		while (counter < this.getNumOfPlaces()) {
            int temp=0;
            if(counter==0){
                if(game.havePort(this.getId())){
                    temp=(int)rand.nextDouble()*(Utils.index(Job.SHIP_CAPTAIN,jobs)+1)+Utils.index(Job.SAILOR,jobs);
                }
                else{
                    temp=(int)rand.nextDouble()*(Utils.index(Job.FLIGHT_ATTENDANT,jobs)+1)+Utils.index(Job.CAPTAIN,jobs);
                }
            }
            else
                temp = (int) (rand.nextDouble() * jobs.length);
			if (Utils.index(jobs[temp].getPlaces().getPlaceType(), types) == -1) {
				// No equal placeType has been created
				types[counter] = jobs[temp].getPlaces().getPlaceType();
				String testimony = "TODO";
				if (Utils.NotInIntArray(this.getId(), caseCities)) {
					int t = (int) rand.nextDouble() * incorrect.getNumOfSentences();
					testimony = (String) incorrect.getMap().values().toArray()[t];
				} else if (this.getId() == caseCities[caseCities.length - 1])
					testimony = ""; // If is the last city the testimony is
									// unnecessary
				else
					testimony = makeTestimony(caseCities, thief, sexVariants, game, player,
							jobs[temp].getPlaces().getPlaceType());

				boolean sex = rand.nextBoolean();
				Witness swapWitness = new Witness(
						Utils.sexVariantsSubstitution(sexVariants, jobs[temp].toString(), sex), sex, jobs[temp],
						testimony);
				this.places[counter++] = new Place(this.getId(), jobs[temp].getPlaces(), swapWitness,game.getPlaceName(jobs[temp].getPlaces(),this.getId()));
			}

		}

	}

	/**
	 * Make a testimony to the witness to bring it to player
	 * 
	 * @param cities
	 *            cities where the player can get clues
	 * @param thief
	 *            thief who the player pursues
	 * @param sexmorphemes
	 *            map to made the substitution
	 * @param game
	 *            database where the clue will get
	 * @param player
	 *            player who is playing
	 * @param type
	 *            type of the place to make a testimony related with the place
	 * @return the string made
	 */
	private String makeTestimony(int[] cities, Thief thief, Map<String, String> sexmorphemes, GameDatabase game,
			Detective player, PlaceType type) {
		if(BuildConfig.DEBUG) {
			assert !Utils.NotInIntArray(this.getId(),
					cities) : "This functions may be called only when the city is in the case cities";
		}
        Level l = player.getRank().getLevel();
        if(type == PlaceType.TRAVEL)
            l=Level.EASY;
		String string = game.clueCity(cities[Utils.indexInt(this.getId(), cities) + 1], l, type) +" "
				+ game.clueThief(player.getRank().getLevel(), thief);
		string = Utils.sexVariantsSubstitution(sexmorphemes, string, thief.getSex());
		return string;
	}

	// Getters
	/**
	 * Get the number of places.
	 * 
	 * @return number of places
	 */
	public int getNumOfPlaces() {
		return this.places.length;
	}

	/**
	 * Get the place inside the places array.
	 * 
	 * @param i
	 *            index in array
	 * @return place
	 */
	public Place getPlace(int i) {
		if (i < this.places.length && i >= 0)
			return this.places[i];
		else
			return null;
	}

	/**
	 * Get the number of destinations.
	 * 
	 * @return number of destinations
	 */
	public int getNumOfDestinations() {
		return this.destinations.length;
	}

	/**
	 * Get the destination inside the destinations array.
	 * 
	 * @param i
	 *            index in array
	 * @return destination
	 */
	public int getDestination(int i) {
		if (i < this.destinations.length && i >= 0)
			return this.destinations[i];
		else
			return currentCity;
	}

	/**
	 * {@inheritDoc}.
	 * 
	 * @return {@inheritDoc}
	 */
	@Override
	public String toString() {
		String destinations = "";
		String places = "";
		for (int i = 0; i < this.destinations.length; i++) {
			destinations += this.destinations[i] + ", ";
		}
		for (int i = 0; i < this.places.length; i++) {
			places += this.places[i];
			places += "\n";
		}
		return super.toString() + ". Destinations: " + destinations + "Places: \n" + places;
	}

    /**
     * Get the information if the city is 2 cities after one of correct cities.
     *
     * @return true if is error, false if not;
     */
	public boolean getError(){
        return error;
    }

}
