package com.jkanetwork.st.thiefbusters;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jkanetwork.st.frontend.InterfaceText;
import com.jkanetwork.st.frontend.Variables;
import com.jkanetwork.st.people.utils.Rank;
import com.jkanetwork.st.utils.Utils;

import static com.jkanetwork.st.frontend.Variables.*;

/**
 * Created by joselucross on 24/03/17.
 */

public class Finish extends AppCompatActivity {
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.finish_game);
        ((Button) findViewById(R.id.finish)).setText(main.getSentence("FINISH"));
        String finishText;
        finishText = main.getSentence("BADORDER");
        if(caso.getWarrant()==null)
            finishText = main.getSentence("NOORDER");
        else if(caso.getWarrant().equals(caso.getThief())){
            finishText = main.getSentence("SOLVE");
            caso.getPlayer().increaseCompleted();
            if(caso.getPlayer().getCompleted()== caso.getPlayer().getRank().getTotalCases()) {
                caso.getPlayer().setRank(caso.getPlayer().getRank().getNext());
                finishText +="\n\n";
                String up = main.getSentence("UPGRADE");
                up += Utils.sexVariantsSubstitution(VARIANTS,(new InterfaceText("rank.kson")).getSentence(caso.getPlayer().getRank().name()), caso.getPlayer().getSex());
                finishText+=up+".";
            }else if(caso.getPlayer().getRank()!= Rank.INTERPOL_BOSS){
                String up = main.getSentence("NEED").replace("$m",String.valueOf(caso.getPlayer().getRank().getTotalCases()- caso.getPlayer().getCompleted()));
                up += Utils.sexVariantsSubstitution(VARIANTS,(new InterfaceText("rank.kson")).getSentence(caso.getPlayer().getRank().getNext().name()), caso.getPlayer().getSex());
                finishText+=up+".";
            }
        }
        finishText = finishText.replace("$thief", caso.getThief().getName())
                .replace("$city", currentCity.getName())
                .replace("$firstCountry",Utils.string(caso.loadCity(caso.getCities()[0], player, data, VARIANTS).getCountry()));
        if(caso.getWarrant()!=null)
                finishText = finishText.replace("$orderThief", caso.getWarrant().getName());
        finishText = Utils.sexVariantsSubstitution(VARIANTS,finishText, caso.getThief().getSex());
        ((TextView) findViewById(R.id.textFinish)).setText(finishText);
    }

    public void finishGame(View view){
        finish();
    }
}
