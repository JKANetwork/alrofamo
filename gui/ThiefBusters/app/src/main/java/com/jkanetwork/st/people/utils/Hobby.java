package com.jkanetwork.st.people.utils;
/**
 * Hobby which the thief practice.
 * 
 * @author JKA Network
 */
public enum Hobby {
	TENNIS, GOLF, ALPINISM, VIDEOGAMES, PHOTOGRAPHY;
}
