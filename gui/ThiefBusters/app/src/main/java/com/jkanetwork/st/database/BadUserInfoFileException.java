package com.jkanetwork.st.database;

public class BadUserInfoFileException extends Exception {

	private static final long serialVersionUID = 1171179601569824706L;

	public BadUserInfoFileException(String s){
		super(s);
	}

}
