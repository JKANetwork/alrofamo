package com.jkanetwork.st.places.utils;

/**
 * A real continents in the world
 * 
 * @author JKA Network
 */
public enum SuperContinent {
	EUROPE,AMERICA,AFRICA,ASIA,OCEANIA;
}
