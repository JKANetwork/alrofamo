package com.jkanetwork.st.thiefbusters;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jkanetwork.st.frontend.Variables;
import com.jkanetwork.st.thiefbusters.methods.Methods;

/**
 * Created by joselucross on 23/03/17.
 */

public class CityClue extends AppCompatActivity {

    /* El menu */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu); /* El menu está en res/menu/main.xml */
        Methods.menuStart(this,menu);
        return true;
    }
    /* Aquí lo que hacer al dar a cada botón del menu */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.options:
            case R.id.about:
            case R.id.exit:
                return Methods.optionsMenu(item,this);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreate(Bundle bundle){
        try {
            super.onCreate(bundle);
            setContentView(R.layout.city_clue);
            ((Button) findViewById(R.id.back)).setText(Variables.main.getSentence("BACK"));
            //TODO set images
            Intent intent = getIntent();
            int place = intent.getIntExtra("place", 0);
            String track = Variables.currentCity.getPlace(place).getWitness().getTestimony();
            ((TextView) findViewById(R.id.track)).setText(track);
        }catch (NullPointerException ex){
            finish();
        }
    }

    public void back(View view){
        finish();
    }

}
