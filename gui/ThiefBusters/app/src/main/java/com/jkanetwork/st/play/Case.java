package com.jkanetwork.st.play;

import android.util.Log;

import java.io.Serializable;
import java.util.*;

import com.jkanetwork.st.database.*;
import com.jkanetwork.st.people.*;
import com.jkanetwork.st.places.City;
import com.jkanetwork.st.utils.Utils;
/**
 * All information about one case.
 * 
 * @author JKA Network
 */
public class Case implements Serializable {
	
	//Constants
	//private static final String STATE="Debug";
	/**
	 * Max of cities in a case.
	 */
	public static final int MAX = 7;
	/**
	 * Min of cities in a case.
	 */
	public static final int MIN = 5;
	
	//Attributes
	/**
	 * The thief who stole.
	 */
	private Thief thief;
	/**
	 * The thief who the detective have a warrant.
	 */
	private Thief warrant;
	/**
	 * PC of the game.
	 */
	private PC pc;
	/**
	 * ID of the cities in the case.
	 */
	private int[] cities;
	/**
	 * Player information.
	 */
	private Detective player;
	/**
	 * Cities
	 */
	private Map<Integer,City> city;
	/**
	 * 
	 */
	
	
	//Builder
	/**
	 * Create a case.
	 *
	 * @param variants map with the sex variants for replace
	 * @param pc PC generated in the front to search thieves
	 */
	public Case(Detective player, GameDatabase data,Map<String,String> variants,PC pc){
		//Create player
		this.player = player;
		citiesIDGeneration(data);
		this.thief = data.selectThief();
		this.city = new TreeMap<>();
		this.pc = pc;
		//Made the first city
		City city = data.selectCity(this.cities[0], player.getRank().getLevel());
		city.destinationsGeneration(data, this.cities);
		city.locationGeneration(this.cities, thief, variants, data, player);
		this.city.put(new Integer(cities[0]),city);
	}
	
	/**
	 * Create a case based in a load data.
	 * 
	 * @param detective
	 * @param thief
	 * @param warrant
	 * @param pc
	 * @param cities
	 */
	public Case(Detective detective,Thief thief, Thief warrant, PC pc,int[] cities){
		this.city = new TreeMap<>();
		this.player=detective;
		this.thief=thief;
		this.warrant=warrant;
		this.pc=pc;
		this.cities=cities;
	}
	
	//Generations
	
	/**
	 * Create a vector with all cities's id in that case.
	 */
	private void citiesIDGeneration(GameDatabase gameData){
		int citiesTotal = 0; //Quantity of cities is need to make
		int tempID;
		int numberOfCities = gameData.citiesCount();
        Log.d("CITIES NUMBER",""+numberOfCities);

        switch(player.getRank().getLevel()){
		case EASY:
			citiesTotal = 5;
			break;
		case MEDIUM:
			citiesTotal = 6;
			break;
		case HARD:
			citiesTotal = 7;
			break;
		}
		cities = new int[citiesTotal];

		Random ranNum = new Random();
		for(int i=0;i<cities.length;++i){
			do{
				tempID=(int)(ranNum.nextDouble()*numberOfCities + 1);
			}while(!Utils.NotInIntArray(tempID, cities));
			cities[i] = tempID;
		}
	}
	
	/**
	 * Get the city where the player is. First search it in the Map with the cities, if do not find in it search in user database.
	 * Else made the city.
	 * 
	 * @param id city id
	 * @param variants map with the sex variants for replace
	 * @return the city loaded
	 */
	public City loadCity(int id,PlayerDatabase data, GameDatabase data2, Map<String,String> variants){
		if(this.city.containsKey(id)) {
            City.beforeCity = City.currentCity;
            City.currentCity = id;
            return this.city.get(id);
        }
        else if(data.cityIsSaved(id)) {
            City swap = data.loadCity(id, data2);
            this.city.put(new Integer(id),swap);
            return swap;
        }
		else{
			City city = data2.selectCity(id, this.player.getRank().getLevel());
			city.destinationsGeneration(data2, this.cities);
			city.locationGeneration(this.cities, thief, variants, data2, player);
            data.saveCity(city);
            this.city.put(new Integer(id),city);
			return city;
		}
	}
	
	//Getters
	/**
	 * Get the thief.
	 * 
	 * @return thief
	 */
	public Thief getThief(){
		return this.thief;
	}
	/**
	 * Compare Thief and Thief with order.
	 * 
	 * @return true if are equals, false if not
	 */
	public boolean compareThieves(){
		return this.thief.equals(this.warrant);
	}
	/**
	 * Get the array with city ids.
	 * 
	 * @return array of integer
	 */
	public int[] getCities(){
		return this.cities;
	}
	/**
	 * Get the current player.
	 * 
	 * @return player
	 */
	public Detective getPlayer(){
		return this.player;
	}
	/**
	 * Get the PC.
	 * 
	 * @return pc
	 */
	public PC getPC(){
		return this.pc;
	}
	
	//Setters
	/**
	 * Set a new warrant.
	 * 
	 * @param warrant a new thief who is returned unique by the GameDatabase  
	 */
	public void setWarrant(Thief warrant){
		this.warrant = warrant;
	}

	/**
	 * Get the thief who user belive is the thief.
	 *
	 * @return a thief
	 */
	public Thief getWarrant(){return this.warrant;}
}
