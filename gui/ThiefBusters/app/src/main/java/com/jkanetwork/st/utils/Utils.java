package com.jkanetwork.st.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.List;

import com.jkanetwork.st.people.utils.Level;

public class Utils {
	/**
	 * Check if a value is into an array.
	 * 
	 * @param value
	 *            integer that is wanted to know if is into a array
	 * @param array
	 *            integer array that will be explorer
	 * @return true if value IS NOT in array, false if value IS in array
	 */
	public static boolean NotInIntArray(int value, int[] array) {
		for (int i = 0; i < array.length; ++i) {
			if (value == array[i]) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Get the first index in an array where the element be found.
	 * 
	 * @param value
	 *            element that the user wants
	 * @param array
	 *            array where to look for the element
	 * @return index if value exists, if not exist return -1
	 */
	public static <E> int index(E value, E[] array) {
		for (int i = 0; i < array.length; i++)
			if (array[i] != null && array[i].equals(value))
				return i;
		return -1;
	}

	/**
	 * The implementation of index only for integer
	 * 
	 * @param value
	 *            element that the user wants
	 * @param array
	 *            array where to look for the element
	 * @return index if value exists, if not exist return -1
	 */
	public static int indexInt(int value, int[] array) {
		for (int i = 0; i < array.length; i++)
			if (array[i] == value)
				return i;
		return -1;
	}

	/**
	 * Replace in one string the meta-characters to specify the sex.
	 * 
	 * @param sexmorphemes The map with the keys and the substitutions
	 * @param string The string to modify
	 * @param s the sex to the substitution
	 * @return A new string with the substitution applied
	 */
	public static String sexVariantsSubstitution(Map<String, String> sexmorphemes, String string, boolean s) {

		Iterator<String> it = sexmorphemes.keySet().iterator();
		String sex;
		if (s)
			sex = "FEMALE";
		else
			sex = "MALE";
		while (it.hasNext()) {
			String key = it.next();
			String substitution = sexmorphemes.get(key);
			String[] split = key.split("_");
			if (split.length == 1) {//When the add is only for female (in Spanish is very common add -a -esa or -ina)
				if (s)
					string = string.replace("$" + split[0].toCharArray()[0], substitution);
				else
					string = string.replace("$" + split[0].toCharArray()[0], "");
			} else {
				if(split[1].equals(sex))
					string = string.replace("$" + split[0].toCharArray()[0], substitution);
			}
		}
		return string;
	}

	public static void delay(long ms){
        long start = System.currentTimeMillis();
        while(System.currentTimeMillis()-start<ms);
    }

    public static String string(String s){
        if(s==null)
            return "null";
        else
            return s;
    }

    public static int[] shuffle(int[] ints){
        List<Integer> list = new ArrayList<>(Arrays.asList(intToInteger(ints)));
        Collections.shuffle(list);
        Integer[] r = new Integer[ints.length];
        r = list.toArray(r);
        return integerToint(r);
    }

    public static Integer[] intToInteger(int[] i){
        Integer[] ret = new Integer[i.length];
        for(int j=0;j<i.length;j++){
            ret[j]=new Integer(i[j]);
        }
        return ret;
    }

    public static int[] integerToint(Integer[] i){
        int[] ret = new int[i.length];
        for(int j=0;j<i.length;j++){
            ret[j]=i[j];
        }
        return ret;
    }

    public static String getStringFromArray(int[] array){
		String ret="";
		for(int i = 0; i<array.length;i++){
			ret+=" "+array[i];
		}
		return ret;
	}

}
