
package com.jkanetwork.st.play;

import android.util.Log;

import java.io.Serializable;
import java.util.List;

import com.jkanetwork.st.database.GameDatabase;
import com.jkanetwork.st.people.Thief;

/**
 * PC where put info from thieves.
 * 
 * @author JKA Network
 * @see com.jkanetwork.st.people.Thief
 * @see com.jkanetwork.st.people.utils
 */
public class PC implements Serializable{

	/**
	 * Database with the information in the game
	 */
	private GameDatabase game;

	// Thief data
	public String sex;
	public String hair;
	public String feature;
	public String car;
	public String hobby;

	// Construct
	/**
	 * Create a PC connected to database
	 * 
	 * @param data
	 *            database to connect
	 */
	public PC(GameDatabase data) {
		this.game = data;
	}

	/**
	 * Method what search and set the information of the thief provide in the
	 * PC. <br>
	 * <br>
	 * The information must be given in the next order: <br>
	 * <br>
	 * sex (M,F) <br>
	 * hair (Enum game.people.utils.Hair) <br>
	 * feature (Enum game.people.utils.Feature) <br>
	 * car (Enum game.people.utils.Car) <br>
	 * hobby (Enum game.people.utils.Hobby) <br>
	 * If the user not provide that information must be a null value
	 * 
	 * @param features
	 *            array of string with dimension 5
	 * @return List of thief find in database
	 */
	public List<Thief> setAndSearch(String[] features) {
		assert features.length == 5 : "The information provided is incorrect, please check it.";

		// Set
		this.sex = features[0];
		hair = features[1];
		feature = features[2];
		car = features[3];
		hobby = features[4];


		// Prepare statement
		String sql = "SELECT * FROM Sospechosos ";
        String sentences[] = new String[5];
        int counter=0;

		if (this.sex != null || hair != null || feature != null || car != null || hobby != null)
			sql += "WHERE ";
		if (this.sex != null) {
            int sex = 0;
            if (this.sex.equals("F"))
                sex = 1;
            sentences[counter++] = "sexo = " + sex + " ";
        }
		if (hair != null)
            sentences[counter++] = "pelo = '" + hair + "' ";
		if (feature != null)
            sentences[counter++] = "caracteristica = '" + feature + "' ";
		if (car != null)
            sentences[counter++] = "coche = '" + car + "' ";
		if (hobby != null)
            sentences[counter++] = "hobby = '" + hobby + "' ";
        sql += sentences[0];
        for(int i = 1;i<counter;i++){
            sql += "AND " + sentences[i];
        }

        Log.v("SQL PC",sql);
		return game.getThiefByPC(sql);

	}

	public List<Thief> search(){
		String[] set = {sex,hair,feature,car,hobby};
		return setAndSearch(set);
	}
	
	/**
	 * Set all attributes to null.
	 */
	public void resetPC(){
		sex=null;
		hair=null;
		feature=null;
		car=null;
		hobby=null;		
	}

	@Override
	public String toString(){
		return sex + " " + hair + " " + feature + " " + car + " " + hobby + "\n";
	}

}
