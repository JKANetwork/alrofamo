package com.jkanetwork.st.places.utils;
/**
 * Partial real continent. It will use to travel and select image to draw witness
 * 
 * @author JKA Network
 */
public enum Continent {
	EAST_EUROPE(SuperContinent.EUROPE),//Ortodox culture, Russia include here
	WEST_EUROPE(SuperContinent.EUROPE), FAR_EAST(SuperContinent.ASIA), 
	MIDDLE_EAST(SuperContinent.ASIA), NORTH_AFRICA(SuperContinent.AFRICA), // Arab and Berber cultures
	SOUTH_AFRICA(SuperContinent.AFRICA), NORTH_AMERICA(SuperContinent.AMERICA), 
	SOUTH_AMERICA(SuperContinent.AMERICA), OCEANIA(SuperContinent.OCEANIA);

	// Attributes
	/**
	 * Continent where it is.
	 */
	private SuperContinent superContinent;

	// Builder
	/**
	 * Connect continent with its supercontinent.
	 * 
	 * @param superContinent
	 */
	private Continent(SuperContinent superContinent) {
		this.superContinent = superContinent;
	}
	
	//Getters
	/**
	 * Get a superContinent associated.
	 * 
	 * @return superContinet
	 */
	public SuperContinent getSuperContinent(){
		return this.superContinent;
	}
}
