BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `Lugares` (
	`IDC`	INTEGER,
	`lugar1`	INTEGER,
	`lugar2`	INTEGER,
	`lugar3`	INTEGER,
	`lugar4`	INTEGER,
	`lugar5`	INTEGER,
	PRIMARY KEY(`IDC`),
	FOREIGN KEY(`IDC`) REFERENCES Ciudadesguardadas ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS `Lugar` (
	`idc`	INTEGER,
	`idl`	INTEGER,
	`nombre`	TEXT,
	`testigoNombre`	TEXT,
	`testigoTrabajo`	TEXT,
	`testigoSexo`	TEXT,
	`testigoTestimonio`	TEXT,
	PRIMARY KEY(`idc`,`idl`),
	FOREIGN KEY(`idc`) REFERENCES Ciudadesguardada ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS `Destinos` (
	`IDC`	INTEGER,
	`destino1`	INTEGER,
	`destino2`	INTEGER,
	`destino3`	INTEGER,
	`destino4`	INTEGER,
	`destino5`	INTEGER,
	PRIMARY KEY(`IDC`),
	FOREIGN KEY(`IDC`) REFERENCES Ciudadesguardadas ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS `Ciudadesguardadas` (
	`IDC`	INTEGER,
	`destinosNum`	INTEGER,
	`lugaresNum`	INTEGER,
	PRIMARY KEY(`IDC`)
);
COMMIT;
