#!/usr/bin/python3
import sqlite3
import random
import time
import os.path
import shutil #Copiar ficheros

## Clases
class Cities(object):
	#Atributos construidos
	def __init__(self,id,name,country,continent,description):
		#Valores extraidos de la base de datos
		self.id = id
		self.name = name
		self.country = country
		self.continent = continent
		#self.locations = locations[:]
		self.description = description
	#Atributos no construidos
	# especificos del juego
	destinations = None #Ciudades que se podrán visitar desde la ciudad actual
	locationsTestimonials = None
	"""Pistas que tendrán los lugares, 
	los indices tienen que ser los mismos que locationsPlayables"""
	locationsPeople = None #Personas que habrá en cada sitio, los indicies son importantes
class Suspect(object):
	def __init__(self):
		self.name = ""
		self.sex = True #True Female, False Male
		self.nacionality = ""
		self.hair = ""
		self.hobby = ""
		self.car = ""
		self.feature = ""
		self.other = ""

ciudad = None
suspect = Suspect()
ciudadAnterior = None # La ciudad Anterior será None siempre que ciudad.id sea == ciudadescaso[0], es importante para las ciudades generadas insitu
citiesCase = [] #Vector de todas las ciudades del caso
ciudadescaso = []
posicioncaso = 0
rango = None #Todas las ciudades posibles

conn = sqlite3.connect('data/gamedata/data.csd') #Conexion a sqlite
ids = conn.execute("SELECT COUNT(ID) FROM Ciudades")
ids = ids.fetchone()
ids = ids[0] #Contiene la cantidad de ids de ciudades, empieza en 1
pistasql = "Aqui hay una pista"

## Aqui todas las funciones

def generaDestinos(idcity):
	global ciudadAnterior
	global rango
	global ciudadescaso
	retorno = []
	numcities = rango[:] #Ids de ciudades jugables
	del numcities[numcities.index(idcity)]
	for x in ciudadescaso:
		try:
			numcities.index(x)
		except ValueError: #Si no existe 
			pass
		else: #Si existe
			del numcities[numcities.index(x)]
	random.shuffle(numcities) #Ids de ciudades jugables
	if ciudadAnterior != None:
		for y in ciudadescaso:
			if idcity == y and idcity != ciudadescaso[int(len(ciudadescaso)-1)]:
				createlocations = 1
				retorno.append(ciudadescaso[ciudadescaso.index(y)+1])
				break
		else:
			for y in ciudadescaso:
				if ciudadAnterior == y:
					createlocations = 2
					break
			else:
				createlocations = 0
		retorno.append(ciudadAnterior)
	else:
		createlocations = 2
		retorno.append(ciudadescaso[1])
	ciudadEquivocada = False
	if ciudadAnterior == None:
			ciudadAnterior = 0 # No existe una ciudad con esa id
	while createlocations > 0:
		for x in numcities:
			if x != ciudadAnterior:
				retorno.append(x)
				del numcities[numcities.index(x)]
				createlocations -=1
				break
	random.shuffle(retorno)
	return retorno

def generarCiudad(idcity):
	volcadodb = conn.execute("SELECT ID, Nombre,Pais,Continente,Lugares from Ciudades WHERE ID=" + str(idcity)) #Sacamos los datos de la ciudad
	volcadodb = volcadodb.fetchone()
	id = idcity
	name = volcadodb[1] #Aqui volcamos la primera ciudad entera
	country = volcadodb[2]
	continent = volcadodb[3]
	city = Cities(id,name,country,continent,"Future description")
	#temporaloc = volcadodb[4].split(',')
	#city.locations[0] = temporaloc[2]
	clue = False
	contador=0
	for x in ciudadescaso:
		if idcity == x:
			clue = True
			posicionActual=contador
			break
		contador += 1
	if clue == True:
		city.locationsTesimonials = [] #TODO
	else:
		city.locationsTesimonials = [] #TODO
	city.locationsPeople = [] #TODO		

	#Buscamos en la base de datos..
	cityexists = sqliteusuario.execute("SELECT IDCiudad,Destinations FROM Ciudadesguardadas WHERE IDCiudad=" + str(idcity))
	cityexists = cityexists.fetchone()
	
	if cityexists == None: #Genera la nueva ciudad
		city.destinations = generaDestinos(idcity) #Destinos
		savesqldestinations = str(city.destinations).replace(' ','').replace('[','').replace(']','') #Arreglos
		#Ahora mete los resultados en la base de datos, asi ya esta generada y guardada, por si vuelve.
		sqliteusuario.execute("INSERT INTO Ciudadesguardadas (IDCiudad,Destinations) VALUES ('" + str(idcity) + "','" + str(savesqldestinations) + "')") 
	else: #Ya tiene los datos de la DB
		city.destinations = cityexists[1].split(",")		
	
	return city


def generarcaso():
	global ciudad
	global rango
	global ciudadescaso
	rango = list(range(1, ids, 1)) #Esto saca todas las ciudades (ids), pares, que hay
	random.shuffle(rango) #Desordena la lista de ciudades aleatoriamente
	ciudadescaso = rango[:4] #Saca las ciudades para el caso
	ciudad = generarCiudad(ciudadescaso[0]) #Primera ciudad del caso

	
def lugarpista(): #Esto da la pista de la ciudad
	lugmenu=True
	while lugmenu:
		print ("Estos lugares hay aquí:")
		contador = 0
		for x in range(1, int (len(ciudad.locations) / 2)+1): #Array de lugares de la ciudad concreta
			print ( str(x) + ": " + ciudad.locations[contador])
			contador += 2
		lugmenu=input("¿A donde vamos?") 
		if lugmenu.isnumeric(): 
			print("\n ¡Hola!\n "+ pistasql) #La pista
			lugmenu='' #para que vuelva al menu principal
		else:
			print("\n Algo has escrito mal") 


def aeropuerto():
	global ciudad
	global ciudadAnterior
	destinos = []
	for x in ciudad.destinations:
		variable = conn.execute("SELECT ID, Nombre FROM Ciudades WHERE ID=" + str(x))
		variable = variable.fetchone()
		destinos.append(variable[1]) #Ir añadiendo los destinos
	ans=True
	while ans:
		for x in destinos:
			print(str(destinos.index(x)+1) + ": " + x)
		ans=input("¿A dónde quieres ir?")
		if ans.isnumeric():
			if int(ans) <= len(destinos):
				ciudadAnterior = ciudad.id
				ciudad = generarCiudad(ciudad.destinations[int(ans)-1])
				ans = False
		else:
			print ("Opcion incorrecta")
## Empieza el juego MUAHAHA
usuario = input("¿Cual es tu nombre? ")

#Esto va a generar un nuevo usuario o cargar el anterior
if os.path.exists('data/playerdata/' + usuario + '.csd'):
	sqliteusuario = sqlite3.connect('data/playerdata/' + usuario + 
'.csd') #Conexion a sqlite
else:
	shutil.copyfile('data/playerdata/emptyplayer.csd', 'data/playerdata/' 
+ usuario + '.csd')
	sqliteusuario = sqlite3.connect('data/playerdata/' + usuario + '.csd') #Conexion a sqlite

generarcaso() # De momento, genero el caso al arrancar.

## Este es el "loop" principal, el menu, de aqui no puede salir el juego. Llama a las diferentes funciones internas.
ans=True
while ans:
	print ("Te encuentras en ",end="")
	print(ciudad.name)
	print ("Mision: " + str(ciudadescaso))
	print ("numcities: " + str(ciudad.destinations))
	print ("""
	Menu principal:
	1.Aeropuerto
	2.Ir a algun lugar
	3.Policia
	4.Salir
	5.Borrar partida
	""")
	ans=input("¿Que quieres hacer?") 
	if ans=="1": 
		aeropuerto() #Para cambiar de ciudad
	elif ans=="2":
		lugarpista() #Esto le dara una pista
	elif ans=="3":
		print("\n Policia policia..") 
	elif ans=="4":
		print("Adios")
		ans=''
	elif ans=="5":
		os.remove('data/playerdata/' + usuario + '.csd')
		exit()
	elif ans !="":
		print("\n Algo has escrito mal") 
